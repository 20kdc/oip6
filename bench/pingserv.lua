--
-- bench: testbench
--
-- Written starting in 2020.
--
-- To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
-- 

-- driver test

local driver = ...

local addr = require("oip6-addr")
local data = require("oip6-data")

local meG, meLl, meLp = driver.getAddresses()

driver.addListener(function (packet, fromSelf)
	if fromSelf then return end
	-- errors not caught on purpose, but be aware
	local pkt = data.decPacketDeep({
		header = data.proto.ip6,
		raw = packet
	})
	io.stderr:write("packet hdr " .. pkt.payload.header .. ": " .. addr.stringify(pkt.src) .. " -> " .. addr.stringify(pkt.dst) .. "\n")
	local packetRE = data.encPacket(pkt, pkt).raw
	if packetRE ~= packet then
		local failed = io.open("failed.bin", "wb")
		failed:write(packetRE)
		failed:close()
		error("data mismatch on re-encode test")
	end
	if pkt.dst == meG or pkt.dst == meLl or pkt.dst == meLp then
		if pkt.payload.header == data.proto.udp then
			io.stderr:write(" port " .. pkt.payload.srcPort .. " -> " .. pkt.payload.dstPort .. "\n")
			io.stderr:write(" data: " .. pkt.payload.data .. "\n")
			local packetDec = {
				header = data.proto.ip6,
				metadata = 0,
				hopLimit = 64,
				src = pkt.dst,
				dst = pkt.src,
				payload = {
					header = data.proto.udp,
					srcPort = pkt.payload.dstPort,
					dstPort = pkt.payload.srcPort,
					data = "your thoughts are appreciated\n"
				}
			}
			driver.transmit(data.encPacket(packetDec, packetDec).raw)

			local packetDec = {
				header = data.proto.ip6,
				metadata = 0,
				hopLimit = 64,
				src = pkt.dst,
				dst = pkt.src,
				payload = pkt
			}
			driver.transmit(data.encPacket(packetDec, packetDec).raw)

			-- If you send a packet on destination port 500, then it's considered valid.
			-- Otherwise, it isn't.
			-- This makes a good test for the Deadport Daemon.
			return pkt.payload.dstPort == 500
		end
	end
end)

