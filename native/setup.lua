#!/usr/bin/lua

--
-- oip6-native-setup: reference/assistant to help setup routing
--
-- Written starting in 2020.
--
-- To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
-- 


-- Execute with sudo.
-- Verify actions before actually running this.
-- If something goes wrong, you'll then know how to undo any issues that result.

package.path = package.path .. ";../common/?.lua"

local addr = require("oip6-addr")
local cfg = require("oip6-native-config")

addr.parsePrefix(cfg.route)

os.execute("ip tuntap add mode tun name " .. cfg.interior .. " user " .. cfg.user)
os.execute("ip link set " .. cfg.interior .. " up")
os.execute("ip -6 route add " .. cfg.route .. " dev " .. cfg.interior)
os.execute("sysctl net.ipv6.conf.all.forwarding=1")

dofile("oip6-native-deployment.lua")

print([[
Your computer is now acting as gateway, but there will be link-layer issues if your network has 'parent' routers.
Specifically, you need to configure routes with next-hop on every single system on your LAN,
OR you need some other way of providing routes.
I tried Router Advertisements, but they don't have the options I need to put in next-hop.
So, rather than do that, use something like https://github.com/DanielAdolfsson/ndppd to resolve.
Also, please thank them for saving me tons and tons of manual configuration.

The following example configuration is for the version in the Ubuntu repositories, 0.2.5-4.

NOTES:
1. Proxy interfaces are those on which ndppd sends Neighbour Advertisements.
   (i.e. the LAN)
   They are NOT the interfaces that you are creating!

2. The 'iface' directive does specify the target interface.

   However, 'static' works as well.
    (but is considered bad according to the manual page,
     due to the creation of 'spurious neighbor entries')

   It is useful if ICMPv6 is not functioning properly within the game world,
    or for efficiency reasons, or in case you make things complicated.
]])

local ifaces = io.open("/proc/net/dev", "r")

-- header
ifaces:read()
ifaces:read()
--

while true do
	local ifn = ifaces:read()
	if not ifn then break end
	local e = ifn:find(":") - 1
	ifn = ifn:sub(1, e)
	while ifn:sub(1, 1) == " " do ifn = ifn:sub(2) end
	if ifn ~= cfg.interior and ifn ~= "lo" then
print([[
proxy ]] .. ifn .. [[ {
	rule ]] .. cfg.route .. [[ {
		iface ]] .. cfg.interior .. [[

	}
}
]])
	end
end

print("")
print("Finally, please remember; if using ndppd,")
print(" and this is the first run of the script this boot,")
print(" you will need to restart ndppd, as the interface has")
print(" been lost.")
