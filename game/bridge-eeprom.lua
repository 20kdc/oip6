-- Tunio Bridge Microcontroller
-- Requires target such as "localhost:1476" as label
local e = component.proxy(component.list("eeprom", true)())
local m = component.proxy(component.list("modem", true)())
local i = component.proxy(component.list("internet", true)())

local PORT = 1476
m.open(PORT)
local tcp = i.connect(e.getLabel())
local buf = ""

while true do
	while true do
		local b, e = tcp.read(2048)
		if not b then
			-- ERROR --
			local retryTmr = computer.uptime() + 10
			while computer.uptime() < retryTmr do computer.pullSignal(1) end
			computer.shutdown(true)
			break
		elseif b == "" then
			break
		else
			buf = buf .. b
			while #buf >= 4 do
				local n = 0
				n = n + (buf:byte(1) * 0x1000000)
				n = n + (buf:byte(2) * 0x10000)
				n = n + (buf:byte(3) * 0x100)
				n = n + (buf:byte(4) * 0x1)
				if #buf >= n then
					m.broadcast(PORT, buf:sub(5, n))
					buf = buf:sub(n + 1)
				else
					break
				end
			end
		end
	end

	local tp, x, y, tb, tc, td = computer.pullSignal(0)
	if tp == "modem_message" and tb == PORT and x ~= y then
		local n = (#td) + 4
		local hdr = ""
		n, hdr = math.floor(n / 256), string.char(n % 256) .. hdr
		n, hdr = math.floor(n / 256), string.char(n % 256) .. hdr
		n, hdr = math.floor(n / 256), string.char(n % 256) .. hdr
		n, hdr = math.floor(n / 256), string.char(n % 256) .. hdr
		tcp.write(hdr .. td)
	end
end
