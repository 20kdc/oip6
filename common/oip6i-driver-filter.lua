--
-- oip6-driver-filter: IPv6 Driver Sanity Filtering & Fragment Reassembly
--
-- Written starting in 2020.
--
-- To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
-- 

local data = require("oip6-data")

-- config [
-- The maximum payload size of a reassembled packet.
-- Do not; repeat, DO NOT; set above 65535, unless you want your system to be
--  remotely crashable.
local MAX_FASM_SIZE = 8192
-- The amount of complete packets that can be going through reassembly.
local MAX_FASM_ENTS = 4
-- ]

-- NOTE: getAddresses may be unsafe at this time.
-- It must be complete by the time the first packet comes in.
-- realTransmit is the real transmission function.
return function (getAddresses, realTransmit, realReceive, warn)

	-- This is the set of fragment reassembly slots.
	-- They consist of {mapId, dataMap, fillMap, hasEnd, fzH, fzT}.
	local faSlots = {}
	local faRevolver = 0
	-- This maps map IDs (src .. dst .. fragTail.id) to slots.
	local faMap = {}

	local function faReceiveSpecific(slot, fragHead, fragTail, fromSelf)
		local obj = faSlots[slot]

		local fragPos = fragTail.fragmentOffset
		local fragLen = #fragTail.data
		local fragEnd = ((fragTail.fragmentCtrl % 2) == 0)
		--warn("frag: " .. fragPos .. " " .. fragLen .. " " .. tostring(fragEnd))

		if fragPos == 0 then
			obj[5] = fragHead
			obj[6] = fragTail
		end

		-- ensure map is of correct size
		local necessaryLen = fragPos + fragLen
		if necessaryLen > MAX_FASM_SIZE then
			warn("frag: fragmented packet would be larger than MAX_FASM_SIZE")
			return true
		end
		local expansion = ("\x00"):rep(#obj[3] - necessaryLen)
		obj[2] = obj[2] .. expansion
		obj[3] = obj[3] .. expansion
		-- data into map
		obj[2] = obj[2]:sub(1, fragPos + 1) .. fragTail.data         .. obj[2]:sub(necessaryLen + 1)
		obj[3] = obj[3]:sub(1, fragPos + 1) .. ("\xFF"):rep(fragLen) .. obj[3]:sub(necessaryLen + 1)
		-- end
		obj[4] = obj[4] or fragEnd

		-- are we done?
		if obj[4] and obj[5] then
			if obj[3] == ("\xFF"):rep(#obj[3]) then
				-- we're done! FINALLY!
				-- hahaha no it has only just begun
				local zHead, zTail = obj[5], obj[6]
				local nh = zTail.nextHeader
				-- wipe zTail and replace it to complete the packet
				for k, v in pairs(zTail) do
					zTail[k] = nil
				end
				zTail.header = nh
				zTail.raw = obj[2]
				realReceive(data.encPacket(zHead).raw, fromSelf)
				return true
			end
		end
	end

	local function faReceive(src, dst, packet, fromSelf)
		local encap = {
			header = data.proto.ip6,
			raw = packet
		}
		local headerType, headerPos = data.findContentHeader(encap)
		if headerType == data.proto.ip6frag then
			-- Fragment! Ok, this needs to be analyzed further.
			local ok, fragHead = data.decPacketDeepSafe(encap)
			if not ok then
				-- debug message here?
				warn("frag: findContentHeader worked but decPacketDeepSafe didn't (A)")
				warn(tostring(fragHead))
				return
			end
			local fragTail = fragHead
			while fragTail.payload do
				fragTail = fragTail.payload
			end
			if fragTail.header ~= data.proto.ip6frag then
				warn("frag: findContentHeader worked but decPacketDeepSafe didn't (B) ")
				return
			end
			local mapId = src .. dst .. fragTail.id
			-- Fragment Assembly slot management
			local slot = faMap[mapId]
			if not slot then
				slot = faRevolver
				if faSlots[slot] then
					faMap[faSlots[slot][1]] = nil
					-- slot itself will be overwritten shortly
				end
				faRevolver = (faRevolver + 1) % MAX_FASM_ENTS
				faSlots[slot] = {mapId, "", "", false, false, false}
				faMap[mapId] = slot
			end
			if faReceiveSpecific(slot, fragHead, fragTail, fromSelf) then
				-- success or error
				faSlots[slot] = nil
				faMap[mapId] = nil
			end
		else
			-- Ok, not fragmented, pass as-is
			realReceive(packet, fromSelf)
		end
	end

	local function getPacketSrcDst(packet)
		return packet:sub(9, 24), packet:sub(25, 40)
	end

	return function (packet)
		-- Transmit
		local myGlobalAddress, myLinkLoAddress, loopbackAddress = getAddresses()
		--
		local src, dst = getPacketSrcDst(packet)
		-- Output Local
		repeat
			-- source multicast
			if src:byte(1) == 255 then break end
			-- dest not self-addressed or multicast
			if dst ~= myGlobalAddress and dst ~= myLinkLoAddress and dst ~= loopbackAddress and dst:byte(1) ~= 255 then break end
			faReceive(src, dst, packet, true)
		until true
		-- Output Remote
		repeat
			-- dest self-addressed
			if dst == myGlobalAddress or dst == myLinkLoAddress or dst == loopbackAddress then break end
			-- dest multicast interface-local
			if ((dst:byte(1) == 255) and ((dst:byte(2) % 16) == 1)) then break end
			realTransmit(packet)
		until true
	end,
	function (packet)
		-- Sanity pre-filter (in case ping packets are still enabled)
		if #packet < 40 then return end

		-- Receive
		local myGlobalAddress, myLinkLoAddress, loopbackAddress = getAddresses()
		--
		-- Confirm that the source isn't blatantly stupid
		local src, dst = getPacketSrcDst(packet)
		if src == myGlobalAddress or src == myLinkLoAddress or src == loopbackAddress or src:byte(1) == 255 then
			return
		end
		-- The packet must be going vaguely in this direction to prevent waking up the listeners for no reason
		-- Plus, this filters out interface-local spoofing
		local targetHere =
			((dst:byte(1) == 255) and ((dst:byte(2) % 16) ~= 1)) or -- multicast & not interface-local
			(dst == myGlobalAddress) or -- self (unicast)
			(dst == myLinkLoAddress) -- self (link-local)

		if targetHere then
			faReceive(src, dst, packet, false)
		end
	end
end
