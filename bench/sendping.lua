--
-- bench: testbench
--
-- Written starting in 2020.
--
-- To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
-- 

-- sort of a test pingy thing

local driver = ...

local addr = require("oip6-addr")
local data = require("oip6-data")

local meG, meLl, meLp = driver.getAddresses()

local target = addr.parse(os.getenv("T"))

driver.addListener(function (packet, fromSelf)
	if fromSelf then return end
	-- errors not caught on purpose, but be aware
	local pkt = data.decPacketDeep({
		header = data.proto.ip6,
		raw = packet
	})
	io.stderr:write("packet hdr " .. pkt.payload.header .. ": " .. addr.stringify(pkt.src) .. " -> " .. addr.stringify(pkt.dst) .. "\n")
end)

local function sendPing()
	driver.at(driver.time() + 2, sendPing)
	driver.transmit(data.encPacket({
		header = data.proto.ip6,
		src = meG,
		dst = target,
		payload = {
			header = data.proto.icmp6,
			paramType = 128,
			paramCode = 0,
			-- in case you're wondering, this is the first time this stack was used over the Internet.
			data = "To Skye: Hi! Here's that ping you asked for. Did you know? ICMP6 allows for rambling! Real rambling!"
		}
	}).raw)
end

sendPing()

