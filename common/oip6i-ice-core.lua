--
-- oip6-ice-core: Packet routing for oip6-ice
--
-- Written starting in 2020.
--
-- To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
--

local dataLib = require("oip6-data")
local addrLib = require("oip6-addr")
local tcpMachine = require("oip6i-tcp")

-- config = {
--  mdnsName = "lab01.local",
--  dns = "2606:4700:4700::1111"
-- }
return function (driver, config, inetReady)
	local ownedLocks = {}

	local function lockPort(proto)
		for j = 1, 8 do
			local i = 32767 + math.random(32767)
			if not driver.lock(proto .. "." .. i) then
				ownedLocks[i] = true
				return i
			end
		end
		for i = 32768, 65535 do
			if not driver.lock(proto .. "." .. i) then
				ownedLocks[i] = true
				return i
			end
		end
	end

	local gameId = 0
	local function newGameId()
		gameId = gameId + 1
		return "ca4e-" .. gameId
	end

	local function dnsTransform(name)
		local oName = name
		name = name .. "."
		local result = ""
		local resultBuffer = ""
		for i = 1, #name do
			local ch = name:sub(i, i)
			if ch == "." then
				if resultBuffer ~= "" then
					result = result .. string.char(#resultBuffer) .. resultBuffer
					resultBuffer = ""
				else
					error("invalid DNS name: " .. oName)
				end
			else
				resultBuffer = resultBuffer .. ch
			end
		end
		return result .. "\x00"
	end

	local mdnsName = nil
	if config.mdnsName then
		mdnsName = dnsTransform(config.mdnsName)
	end

	local function dnsNameLen(buf, at)
		-- not sure WHAT this is
		if buf:sub(at, at + 1) == "\xc0\x0c" then
			return 2, at + 2
		end
		local len = 0
		while true do
			local sl = buf:byte(at)
			len = len + 1
			at = at + 1
			if sl == 0 then
				return len, at
			end
			at = at + sl
			len = len + sl
		end
	end

	local function genId(myAddr, myPort, rAddr, rPort)
		return myAddr .. dataLib.genU16(myPort) .. rAddr .. dataLib.genU16(rPort)
	end

	local tcpConnections = {}
	local tcpListeners = {}
	-- {name, text}
	local allDNSQueries = {}

	local function coreTCP(gameId, myAddr, myPort, rAddr, rPort, inboundPacket)
		local id = genId(myAddr, myPort, rAddr, rPort)
		local hasRemoteClosed = false
		local recvBuffer = ""
		local tPkt, tSend, tClose = tcpMachine(driver.time, driver.at, function (packet)
			packet.header = dataLib.proto.tcp
			packet.windowSize = 65535
			packet.srcPort = myPort
			packet.dstPort = rPort
			local pktDec = {
				header = dataLib.proto.ip6,
				src = myAddr,
				dst = rAddr,
				payload = packet
			}
			driver.transmit(dataLib.encPacket(pktDec, pktDec).raw)
		end, function (a, b)
			if a == "data" then
				recvBuffer = recvBuffer .. b
				inetReady(gameId)
			elseif a == "close" then
				hasRemoteClosed = true
				inetReady(gameId)
			elseif a == "dead" then
				hasRemoteClosed = true
				tcpConnections[id] = nil
				inetReady(gameId)
			end
		end, not not inboundPacket)
		if inboundPacket then
			tPkt(inboundPacket)
		end
		tcpConnections[id] = tPkt
		local obj = {
			id = function ()
				return gameId
			end,
			finishConnect = function ()
				return true
			end,
			read = function (len)
				if recvBuffer == "" and hasRemoteClosed then return nil, "connection closed" end
				len = len or #recvBuffer
				local seg = recvBuffer:sub(1, len)
				recvBuffer = recvBuffer:sub(len + 1)
				return seg
			end,
			write = function (data)
				tSend(data)
				return #data
			end,
			close = function ()
				tClose()
			end
		}
		return obj
	end

	local function coreTCPListener(myAddr, myPort)
		if driver.lock("tcp." .. myPort) then
			return nil, "port already active: " .. myPort
		end
		local id = genId(myAddr, myPort, "", 0)
		local gameId = newGameId()
		local queue = {}
		local queueById = {}
		tcpListeners[id] = function (hdr, content)
			if not content.flags.syn then
				-- unless a handshake is begun, it's a random packet
				return false
			end
			local cId = genId(hdr.dst, content.dstPort, hdr.src, content.srcPort)
			if queueById[cId] then
				return true
			end
			local obj = {hdr, content, cId}
			table.insert(queue, obj)
			queueById[cId] = obj
			inetReady(gameId)
			return true
		end
		return {
			id = function ()
				return gameId
			end,
			waiting = function ()
				if queue[1] then
					return addrLib.stringify(queue[1][1].src)
				end
			end,
			accept = function ()
				local inf = table.remove(queue, 1)
				if not inf then return end
				return coreTCP(newGameId(), inf[1].dst, inf[2].dstPort, inf[1].src, inf[2].srcPort, inf[2])
			end,
			deny = function ()
				local queueEnt = table.remove(queue, 1)
				queueById[queueEnt[3]] = nil
				local ip6, cont = queueEnt[1], queueEnt[2]
				local pktDec = {
					header = dataLib.proto.ip6,
					src = ip6.dst,
					dst = ip6.src,
					payload = {
						header = dataLib.proto.tcp,
						srcPort = cont.dstPort, dstPort = cont.srcPort,
						seqNum = (cont.ackNum + 1) % 0x100000000, ackNum = (cont.seqNum + 1) % 0x100000000,
						flags = {rst = true, ack = true},
						windowSize = 0, data = ""
					}
				}
				driver.transmit(dataLib.encPacket(pktDec, pktDec).raw)
			end,
			close = function ()
				driver.unlock("tcp." .. myPort)
				tcpListeners[id] = nil
			end
		}
	end

	local listener = function (packet, fromSelf, hlType, hlPos)
		if hlType == dataLib.proto.tcp or hlType == dataLib.proto.udp then
			local hdr = dataLib.decPacket({header = dataLib.proto.ip6, raw = packet})
			local content = dataLib.decPacket({header = hlType, raw = packet:sub(hlPos)}, hdr)

			if hlType == dataLib.proto.tcp then
				local id = genId(hdr.dst, content.dstPort, hdr.src, content.srcPort)
				local idListen = genId(hdr.dst, content.dstPort, "", 0)
				local idListenU = genId(addrLib.unspecified, content.dstPort, "", 0)
				if tcpConnections[id] then
					tcpConnections[id](content)
					return true
				elseif tcpListeners[idListen] then
					return tcpListeners[idListen](hdr, content)
				elseif tcpListeners[idListenU] and hdr.src:byte(1) ~= 255 then
					return tcpListeners[idListenU](hdr, content)
				end
			else
				local dnsResponse = content.srcPort == 5353 or content.srcPort == 53
				local dnsRequest = content.dstPort == 5353 and hdr.src:byte(1) ~= 255
				-- If this is an [M]DNS server contacting us, always appear to accept
				--  unless the packet is wrong enough that it causes an error
				if dnsResponse or dnsRequest then
					local flags = dataLib.getU16(content.data, 3)
					--driver.debug("ice-core.dns: request or response (" .. tostring(dnsResponse) .. ")")
					local queries = dataLib.getU16(content.data, 5)
					if queries ~= 1 then
						return
					end
					local answers = dataLib.getU16(content.data, 7)
					local at = 13
					local nameLen, nameAft = dnsNameLen(content.data, at)
					--driver.debug("ice-core.dns: " .. at .. " -> " .. nameAft)
					local qName = content.data:sub(at, nameAft - 1)
					at = nameAft
					local qType = dataLib.getU16(content.data, at)
					local qClass = dataLib.getU16(content.data, at + 2)
					at = at + 4
					if dnsRequest and qName == mdnsName and qClass == 1 and qType == 28 and flags < 0x8000 then
						-- MDNS request for the name, respond
						local myGAddr = driver.getAddresses()
						local response = content.data:sub(1, 2) .. "\x81\x00\x00\x01\x00\x01\x00\x00\x00\x00" .. qName .. "\x00\x1c\x00\x01\xc0\x0c\x00\x1c\x00\x01\x00\x00\x00\x40\x00\x10" .. myGAddr
						local pktDec = {
							header = dataLib.proto.ip6,
							src = myGAddr,
							dst = hdr.src,
							payload = {
								header = dataLib.proto.udp,
								srcPort = content.dstPort,
								dstPort = content.srcPort,
								data = response
							}
						}
						local open = true
						for i = 1, 4 do
							driver.at(driver.time() + i, function ()
								if open then
									driver.transmit(dataLib.encPacket(pktDec, pktDec).raw)
								end
							end)
						end
					elseif dnsResponse then
						for i = 1, answers do
							--driver.debug("ice-core.dns: answer parser @ " .. at .. "=" .. tostring(content.data:byte(at)))
							local _, tNameAft = dnsNameLen(content.data, at)
							at = tNameAft
							local aType = dataLib.getU16(content.data, at)
							local aClass = dataLib.getU16(content.data, at + 2)
							local aDataLen = dataLib.getU16(content.data, at + 8)
							--driver.debug("ice-core.dns: response " .. aType .. ":" .. aClass .. "[" .. aDataLen .. "]")
							at = at + 10
							local aData = content.data:sub(at, at + aDataLen - 1)
							at = at + aDataLen
							if aClass == 1 and aType == 28 and #aData == 16 then
								--driver.debug("ice-core.dns:  = " .. addrLib.stringify(aData))
								-- Finally, got an IPv6 address.
								for k, v in ipairs(allDNSQueries) do
									if v[1] == qName then
										v[2](aData)
									end
								end
							end
						end
					end
					return true
				end
			end
		end
	end
	driver.addListener(listener)

	return {
		tcp = function (gameId, rAddr, rPort)
			local myAddr = driver.getAddresses()
			local myPort = lockPort("tcp.")
			if not myPort then
				return nil, "unable to acquire a port"
			end
			return coreTCP(gameId, myAddr, myPort, rAddr, rPort, nil)
		end,
		newGameId = newGameId,
		lockPort = lockPort,
		tcpListener = coreTCPListener,
		-- Proxy for the 'address not known' state
		tcpProxy = function (gameId)
			local target
			local targetError
			local weActuallyWantToClose = false
			local writeBuffer = ""
			return {
				id = function ()
					return gameId
				end,
				finishConnect = function (...)
					if target then return target.finishConnect(...) end
					return true
				end,
				read = function (...)
					if target then return target.read(...) end
					if targetError then return nil, targetError end
					return ""
				end,
				write = function (data)
					if target then
						return target.write(data)
					end
					writeBuffer = writeBuffer .. data
					return #data
				end,
				close = function ()
					if target then
						return target.close()
					end
					weActuallyWantToClose = true
				end
			}, function (targ, err)
				target = targ
				if target then
					target.write(writeBuffer)
					if weActuallyWantToClose then
						target.close()
					end
				else
					targetError = tostring(err)
				end
				writeBuffer = ""
			end
		end,
		dns = function (name, resultCB)
			local dnsAddr = nil
			local dstPort = 53
			if name:sub(#name - 5) == ".local" then
				dnsAddr = addrLib.parse("ff02::fb")
				dstPort = 5353
			elseif config.dns then
				dnsAddr = addrLib.parse(config.dns)
			else
				return
			end
			name = dnsTransform(name)
			-- Format a DNS query.
			local query = string.char(math.random(256) - 1, math.random(256) - 1) .. "\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00" .. name .. "\x00\x1c\x00\x01"

			local myAddr, _, _ = driver.getAddresses()
			local pktDec = {
				header = dataLib.proto.ip6,
				src = myAddr,
				dst = dnsAddr,
				payload = {
					header = dataLib.proto.udp,
					srcPort = 53530,
					dstPort = dstPort,
					data = query
				}
			}
			local dnsPacket = dataLib.encPacket(pktDec, pktDec).raw

			local obj = {name, nil}
			table.insert(allDNSQueries, obj)

			local open = true
			local finished = false
			local responses = {}

			local function finish()
				if finished then return end
				finished = true
				open = false
				for i = 1, #allDNSQueries do
					if allDNSQueries[i] == obj then
						table.remove(allDNSQueries, i)
						break
					end
				end
				resultCB(responses)
			end

			obj[2] = function (addr)
				open = false
				table.insert(responses, addr)
				driver.at(0, finish)
			end

			local start = driver.time()
			for i = 1, 5 do
				local iHere = i
				driver.at(start + ((i - 1) * 2), function ()
					if iHere == 5 then
						finish()
					elseif open then
						driver.transmit(dnsPacket)
					end
				end)
			end
		end,
		close = function ()
			for k, _ in pairs(ownedLocks) do
				driver.unlock(k)
			end
			driver.rmListener(listener)
		end
	}
end
