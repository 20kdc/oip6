--
-- bench: testbench
--
-- Written starting in 2020.
--
-- To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
-- 

-- tcp test

local driver = ...

local addr = require("oip6-addr")
local data = require("oip6-data")
local tcp = require("oip6i-tcp")

local meG, meLl, meLp = driver.getAddresses()

local target = addr.parse(os.getenv("T"))
local targetPort = 22000

local tcpPkt, tcpSend, tcpClose

tcpPkt, tcpSend, tcpClose = tcp(driver.time, driver.at, function (pktDec)

	-- forced params
	pktDec.header = data.proto.tcp
	pktDec.srcPort = 29100
	pktDec.dstPort = targetPort
	pktDec.windowSize = 65535

	local packetDec = {
		header = data.proto.ip6,
		metadata = 0,
		hopLimit = 255,
		src = meG,
		dst = target,
		payload = pktDec
	}
	driver.transmit(data.encPacket(packetDec, packetDec).raw)

end, function (t, d)
	if t == "data" then
		tcpSend(d)
		driver.debug("echoed " .. #d .. " bytes")
	elseif t == "debug" then
		driver.debug(d)
	elseif t == "close" then
		if d then
			driver.debug("closed (rst)")
		else
			driver.debug("closed (fin)")
		end
	end
end, false)

tcpSend("Welcome to Carrotland, the land of the carrots\n")

driver.addListener(function (packet, fromSelf, hlType, hlPos)
	if fromSelf then return end
	-- errors not caught on purpose, but be aware
	local pkt = data.decPacketDeep({
		header = data.proto.ip6,
		raw = packet
	})
	if pkt.dst == meG or pkt.dst == meLl or pkt.dst == meLp then
		if pkt.payload.header == data.proto.tcp then
			tcpPkt(pkt.payload)
		end
	end
	return true
end)

