--
-- oip6-driver-kosneo: IPv6 driver for KittenOS NEO
--
-- Written starting in 2020.
--
-- To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
-- 

local npb = neo.requireAccess("x.neo.pub.base")
npb.lockPerm("x." .. npb.myApi)
local oip6Registrar = neo.requireAccess("r." .. npb.myApi)

local modems = neo.requireAccess("c.modem")
neo.requireAccess("s.h.modem_message")

-- Config instance (stored for use by inet service)
local args = {}

do
	local cfgContent = [[
return {
-- examples of config settings
--  address = "fda5:b1f9:42ba:4e57:54::1",
--  mdnsName = "turner.local",
--  dns = "2606:4700:4700::1111",
--  debug = true,
--  linkPort = 1476
}
]]
	local cfgRead = npb.open("/config.lua", false)
	if not cfgRead then
		local cfgWrite = npb.open("/config.lua", true)
		cfgWrite.write(cfgContent)
		cfgWrite.close()
	else
		cfgContent = cfgRead.read("*a")
		cfgRead.close()
	end
	local serial = require("serial")
	args = assert(serial.deserialize(cfgContent))
end

local oip6Port = args.linkPort or 1476
local internetUUID = "00a7cade-f00d-4000-8000-7636696e6574"
local debugFile

if args.debug then
	debugFile = npb.open("/oip6.log", true)
end

for v in modems.list() do
	v.open(oip6Port)
end

local addrLib = require("oip6-addr")

local driver = {}

local myLinkLoAddress = addrLib.generateLL(os.address())
local myGlobalAddress = myLinkLoAddress
if args.address then
	myGlobalAddress = addrLib.parse(args.address)
end
local loopbackAddress = addrLib.parse("::1")

-- The Shores Of Abstraction --

function driver.debug(text)
	if debugFile then
		debugFile.write(text .. "\n")
	end
end

local timers = {}

driver.time = os.uptime
driver.at = function (at, fn)
	local tag = neo.scheduleTimer(at)
	tag[1] = fn
end

driver.getAddresses = function ()
	return myGlobalAddress, myLinkLoAddress, loopbackAddress
end

-- Interno --

local filterReceive = require("oip6-driver-svc")(driver, function (packet)
	for v in modems.list() do
		v.broadcast(oip6Port, packet)
	end
end)

-- Internet Card Emulation (this uses a requireAccess, which is synchronous, so do this before registering) --

local vdev = neo.requireAccess("x.svc.virtudev", "lamp")
local iceSignal
local ice = require("oip6-ice-svc")(driver, args, function (conn)
	iceSignal("internet_ready", conn)
end)
ice.address = internetUUID
ice.type = "internet"
ice.slot = -1
iceSignal = vdev.install(ice)

-- The Actual Registration --

local function dbgCall(...)
	local ok, res = pcall(fn)
	if not ok then
		driver.debug(tostring(res))
	end
	return ok, res
end

local attached = {}
oip6Registrar(function (pkg, pid, send)
	local userListeners = {}
	local ownsLocks = {}
	local function apiListener(...)
		local r = false
		coroutine.resume(coroutine.create(function (...)
			for k, v in pairs(userListeners) do
				local ok, res = dbgCall(k, ...)
				if ok and res then
					r = true
				end
			end
		end), ...)
		return r
	end
	driver.addListener(apiListener)
	attached[pid] = function ()
		attached[pid] = nil
		for k, v in pairs(ownsLocks) do
			driver.unlock(k)
		end
		driver.rmListener(apiListener)
	end
	return {
		getAddresses = driver.getAddresses,
		debug = driver.debug,
		addListener = function (l)
			assert(type(l) == "function")
			userListeners[l] = true
		end,
		rmListener = function (l)
			userListeners[l] = nil
		end,
		lock = function (id)
			local res = driver.lock(id)
			if not res then
				ownsLocks[id] = true
			end
			return res
		end,
		unlock = function (id)
			driver.unlock(id)
			ownsLocks[id] = nil
		end,
		getLocks = driver.getLocks,
		time = os.uptime,
		at = function (time, fn)
			driver.at(time, function ()
				coroutine.resume(coroutine.create(dbgCall), fn)
			end)
		end,
		transmit = driver.transmit
	}
end)

-- The Loop --

while true do
	local evType, ev1, ev2, ev3, ev4, ev5 = coroutine.yield()
	if evType == "h.modem_message" then
		if ev3 == oip6Port then
			if type(ev5) == "string" then
				filterReceive(ev5)
			end
		end
	elseif evType == "k.procdie" then
		if attached[ev2] then
			attached[ev2]()
		end
	elseif evType == "k.timer" then
		dbgCall(ev1[1])
	end
end
