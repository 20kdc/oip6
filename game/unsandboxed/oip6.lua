--
-- oip6-driver-ocns: IPv6 driver for unsandboxed OpenComputers systems
--
-- Written starting in 2020.
--
-- To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
-- 

-- THIS IS AN RC SCRIPT --
-- for OpenOS, place as /etc/rc.d/oip6.lua
-- for PsychOS, place as /service/oip6.lua
-- An example rc.cfg configuration:
-- enabled = {"oip6"}
-- oip6 = {
--  address = "fda5:b1f9:42ba:4e57:54::1",
--  mdnsName = "turner.local",
--  dns = "2606:4700:4700::1111",
--  debug = "/oip6.log",
--  linkPort = 1476
-- }
-- NOTE: All of these options are optional.

local computer = computer or require("computer")
local component = component or require("component")
local event = event or require("event")

local running = false
local mmCallback, mmInetClose
local internetUUID = "00a7cade-f00d-4000-8000-7636696e6574"

function start()
	if running then return end
	running = true

	-- Knee Deep In The Prerequisites --
	local rc = rc or require("rc")
	local args = args or (rc and rc.cfg and rc.cfg.oip6)
	if not args then
		print("oip6 is not configured...")
		args = {}
	end
	local addrLib = require("oip6-addr")

	local oip6Port = args.linkPort or 1476

	local debugFile
	if args.debug then
		debugFile = io.open(args.debug, "wb")
	end

	local myLinkLoAddress = addrLib.generateLL(computer.address())
	local myGlobalAddress = myLinkLoAddress
	if args.address then
		myGlobalAddress = addrLib.parse(args.address)
	end
	local loopbackAddress = addrLib.parse("::1")

	-- The Shores Of Abstraction --
	local driver = {}

	function driver.debug(text)
		if debugFile then
			debugFile:write(text .. "\n")
			debugFile:flush()
		end
	end

	local function dbgCall(fn, ...)
		local ok, err = pcall(fn, ...)
		if not ok then
			driver.debug(tostring(err))
		end
	end

	driver.time = computer.uptime
	if event.timer then
		-- OpenOS-style
		driver.at = function (at, fn)
			event.timer(math.max(0.001, at - driver.time()), function () dbgCall(fn) end, 1)
		end
	else
		-- PsychOS-style
		driver.at = function (at, fn)
			os.spawn(function ()
				while driver.time() < at do
					coroutine.yield()
				end
				dbgCall(fn)
			end, "oip6-driver timer " .. at)
		end
	end

	driver.getAddresses = function ()
		return myGlobalAddress, myLinkLoAddress, loopbackAddress
	end
	driver.setGlobalAddress = function (ga)
		assert(type(ga) == "string", "address must be a string")
		assert(#ga == 16, "address wasn't 16 bytes")
		myGlobalAddress = ga
	end

	-- Interno --

	-- Implements platform-independent 'singleton' bits of the API,
	--  in particular the innards of the packet listener framework & the Deadport Daemon
	local filterReceive = require("oip6-driver-svc")(driver, function (packet)
		for c in component.list("modem", true) do
			component.invoke(c, "broadcast", oip6Port, packet)
		end
	end)

	local function rawReceive(_, _, _, port, _, data)
		if port == oip6Port then
			if type(data) == "string" then
				dbgCall(filterReceive, data)
			end
		end
	end
	mmCallback = rawReceive
	event.listen("modem_message", rawReceive)

	for c in component.list("modem", true) do
		component.invoke(c, "open", oip6Port)
	end

	package.loaded["oip6-driver"] = driver

	-- Thy Internet Consumed --
	print("your computer's addresses:")
	print(" global " .. addrLib.stringify(myGlobalAddress))
	print(" link   " .. addrLib.stringify(myLinkLoAddress))

	local vcomponent
	pcall(function ()
		vcomponent = require("vcomponent")
	end)
	if not vcomponent then
		print("VComponent not available. Unable to start Internet Card Emulation.")
	else
		local ice = require("oip6-ice-svc")
		local inet, inetClose = ice(driver, args, function (conn)
			computer.pushSignal("internet_ready", uuid, conn)
		end)

		local ok, err = vcomponent.register(internetUUID, "internet", inet)
		if not ok then
			print("Internet Card Emulation unavailable: " .. err)
			inetClose()
		else
			mmInetClose = inetClose
		end
	end
end

function stop()
	if not running then return end
	running = false
	package.loaded["oip6-driver"] = nil
	event.ignore("modem_message", mmCallback)
	mmCallback = nil
	if mmInetClose then
		vcomponent.unregister(internetUUID)
		mmInetClose()
		mmInetClose = nil
	end
end

