--
-- oip6-data: Utilities for IPv6 data.
--
-- Written starting in 2020.
--
-- To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
-- 

local band, bxor

if bit32 then
	-- Lua 5.2
	band = bit32.band
	bxor = bit32.bxor
else
	-- Lua 5.3
	band = load("local a, b = ... return a & b")
	bxor = load("local a, b = ... return a ~ b")
end

--- PART 1: GENERAL ALGORITHMS ---

local function getU16(s, i)
	return (s:byte(i) * 0x100) + s:byte(i + 1)
end

local function getU32(s, i)
	return (getU16(s, i) * 0x10000) + getU16(s, i + 2)
end

local function genU16(v)
	return string.char(math.floor(v / 0x100), v % 0x100)
end

local function putU16(s, i, v)
	return s:sub(1, i - 1) .. genU16(v) .. s:sub(i + 2)
end

local function genU32(v)
	return genU16(math.floor(v / 0x10000)) .. genU16(v % 0x10000)
end

-- Implements the One's Complement Addition as used in various Internet protocols,
--  for two unsigned 16-bit quantities.
-- I should also note: Does not implement it *well.*
-- To be more precise, implements it exactly as well as is required and no more.
local function addOC16(a, b)
	a = a + b
	if a >= 0x10000 then
		-- Note this is 0xFFFF, not 0x10000, to implement the inc-by-1.
		a = a - 0xFFFF
	end
	return a
end

local function addAreaOC16(a, s)
	local sPad = s .. "\x00"
	for i = 1, #s, 2 do
		a = addOC16(a, getU16(sPad, i))
	end
	return a
end

local function decBits(num, meaning)
	local dec = {}
	local pow = 1
	for _, v in ipairs(meaning) do
		if band(num, pow) ~= 0 then
			dec[v] = true
		end
		pow = pow * 2
	end
	return dec
end
local function encBits(dec, meaning)
	local num = 0
	local pow = 1
	for _, v in ipairs(meaning) do
		if dec[v] then
			num = num + pow
		end
		pow = pow * 2
	end
	return num
end

--- PART 2: STRUCTURE PACKERS ---
--
-- Policy:
--  + Error if something is dangerously out of spec.
--  + Checksums *MUST* be tested for validity, and *MUST* be transparent if possible.
--  + It'd be nice to be able to disassemble a packet in full and then reassemble it.
--
-- Extending from these principles, the design is natural.
-- All hail the hub & spoke, now with added design constraints...
--
-- The 'Encoded Packet' Type:
-- An encoded packet has the following structure:
-- {
--  header = 17, -- IANA protocol number
--  raw = "\x00\x00\x00\x00\x00\x00\x00\x00"
-- }
--
-- The 'Decoded Packet' Type:
-- A decoded packet must have the 'header' field, but then the other fields are arbitrary.
-- They are protocol-specific.
-- A decoded packet MUST NOT have the 'raw' field.
-- An example for UDP would be:
-- {
--  header = 17,
--  srcPort = 1234,
--  dstPort = 69,
--  data = "\x00\x02lost.ark\x00octet\x00"
-- }
--
-- Sub-packets:
-- If a decoded packet has the field 'payload', the value must be an Encoded Packet.

local proto = {
	-- This is NOT a complete list. For reference, please see:
	-- https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
	ip6hbho = 0,
	tcp = 6,
	udp = 17,
	ip6 = 41,
	ip6rout = 43,
	ip6frag = 44,
	icmp6 = 58,
	ip6dsto = 60
}

-- Packet decoders.
-- A packet decoder is called with the following arguments:
-- (packetData, phInfo)
-- Returns the decoded packet.
-- The 'phInfo' has the following 'interface', which the IPv6 header packet format must implement:
-- {
--  src = <16-byte string>,
--  dst = <16-byte string>
-- }
-- It represents the *contextual pseudoheader*.

local decoders = {}

-- Packet encoders.
-- A packet encoder is called with the following arguments.
-- (packetDec, phInfo)
-- Returns the raw data.
local encoders = {}

-- Decodes a packet.
local function decPacket(packetEnc, phInfo)
	if not packetEnc.raw then return packetEnc end
	local dec = decoders[packetEnc.header]
	assert(dec, "no decoder available")
	return dec(packetEnc.raw, phInfo)
end
local function decPacketDeep(packetEnc, phInfo)
	local packetDec = decPacket(packetEnc, phInfo)
	if packetDec.payload then
		-- [PSU]
		if packetDec.header == proto.ip6 then
			phInfo = packetDec
		end
		packetDec.payload = decPacketDeep(packetDec.payload, phInfo)
	end
	return packetDec
end
-- Decodes a packet recursively. If ANY error is found, does not return even the partial decode.
local function decPacketDeepSafe(packetEnc, phInfo)
	return pcall(decPacketDeep, packetEnc, phInfo)
end
-- Encodes a packet recursively.
local function encPacket(packetDec, phInfo)
	if packetDec.raw then return packetDec end
	local enc = encoders[packetDec.header]
	assert(enc, "no encoder available")
	return {
		header = packetDec.header,
		raw = enc(packetDec, phInfo)
	}
end

decoders[proto.ip6] = function (s)
	assert(#s >= 40, "packet is too small to be IPv6")
	local md = getU32(s, 1)
	assert(band(md, 0xF0000000) == 0x60000000, "packet version is not v6")
	local payloadLength = getU16(s, 5)
	local payload = s:sub(41, 40 + payloadLength)
	assert(#payload == payloadLength, "packet payload not available")
	return {
		header = proto.ip6,
		-- really an amalgamation of stuff, most of which is pointless
		-- the original metadata also has version, but that had to be 6 anyway
		metadata = band(md, 0x0FFFFFFF),
		-- payload length is 5,6
		-- next header is 7
		-- hop limit
		hopLimit = s:byte(8),
		-- source address
		src = s:sub(9, 24),
		-- destination address
		dst = s:sub(25, 40),
		-- payload
		payload = {
			header = s:byte(7),
			raw = payload
		}
	}
end
encoders[proto.ip6] = function (packetDec, phInfo)
	assert((#packetDec.src) == 16, "address invalid length")
	assert((#packetDec.dst) == 16, "address invalid length")
	local metadata = packetDec.metadata or 0
	local hopLimit = packetDec.hopLimit or 64
	assert(band(metadata, 0xF0000000) == 0, "packet metadata must not have upper bits set")
	-- [PSU]
	-- This known IPv6 header overrides the pseudoheader parameter.
	-- Any routing header business must do the same for this to all work properly.
	-- The decPacketDeep side of things isn't so automatic, though.
	local payload = encPacket(packetDec.payload, packetDec)
	return
		genU32(metadata + 0x60000000) ..
		genU16(#payload.raw) ..
		string.char(payload.header, hopLimit) ..
		packetDec.src ..
		packetDec.dst ..
		payload.raw
end

-- IPv6 Things We Don't Care About --
-- Nice of them to make these bits basically identical.
local function twdcaDecoder(protocol)
	return function (s, phInfo)
		local len = (s:byte(2) + 1) * 8
		return {
			header = protocol,
			content = s:sub(3, len),
			payload = {
				header = s:byte(1),
				raw = s:sub(len + 1)
			}
		}
	end
end

decoders[proto.ip6hbho] = twdcaDecoder(proto.ip6hbho)
decoders[proto.ip6dsto] = twdcaDecoder(proto.ip6dsto)
decoders[proto.ip6rout] = twdcaDecoder(proto.ip6rout)

local function twdcaEncoder(packetDec, phInfo)
	local payload = encPacket(packetDec.payload, phInfo)
	assert((2 + #packetDec.content) % 8 == 0, "content must end on a multiple of 8")
	return string.char(payload.header, math.floor(((2 + #packetDec.content) / 8) - 1)) .. packetDec.content .. payload.raw
end

encoders[proto.ip6hbho] = twdcaEncoder
encoders[proto.ip6dsto] = twdcaEncoder
encoders[proto.ip6rout] = twdcaEncoder

-- IPv6 Fragment --
decoders[proto.ip6frag] = function (s)
	local ofs = getU16(s, 3)
	return {
		header = proto.ip6frag,
		nextHeader = s:byte(1),
		-- reserved is reserved, but archive it anyway for reproduction
		reserved = s:byte(2),
		fragmentOffset = band(ofs, 0xFFF8),
		fragmentCtrl = band(ofs, 0x0007),
		id = getU32(s, 5),
		-- Because the fragment is not a complete packet, it can't be encapsulated via 'payload'.
		data = s:sub(9)
	}
end

encoders[proto.ip6frag] = function (fragmentDec)
	local ofs = fragmentDec.fragmentOffset
	assert(ofs % 8 ~= 0, "fragment offset must be a multiple of 8")
	assert(math.floor(fragmentDec.fragmentCtrl / 8) ~= 0, "fragment control is 3-bit (and 2 of those bits are reserved)")
	ofs = ofs + fragmentDec.fragmentCtrl
	return string.char(fragmentDec.nextHeader, fragmentDec.reserved) .. genU16(ofs) .. genU32(fragmentDec.id) .. fragmentDec.data
end

-- This implements the 'common pseudo-header' business.
local function calcSLCH(pn, s, phInfo, payloadChecksumAt)
	local payloadEnc = putU16(s, payloadChecksumAt, 0)
	local checksum = addAreaOC16(0, phInfo.src .. phInfo.dst)
	checksum = addOC16(checksum, #payloadEnc)
	checksum = addOC16(checksum, pn)
	checksum = addAreaOC16(checksum, payloadEnc)
	return checksum
end

-- ICMP --
decoders[proto.icmp6] = function (s, phInfo)
	assert(#s >= 4, "ICMPv6 packets are at least 4 bytes")
	local checksum = getU16(s, 3)
	local checksumCorrect = bxor(calcSLCH(proto.icmp6, s, phInfo, 3), 0xFFFF)
	assert(checksum == checksumCorrect, "ICMPv6 checksum failure")
	return {
		header = proto.icmp6,
		paramType = s:byte(1),
		paramCode = s:byte(2),
		-- checksum 3,4
		data = s:sub(5)
	}
end

encoders[proto.icmp6] = function (packetDec, phInfo)
	local raw = string.char(packetDec.paramType, packetDec.paramCode, 0, 0) .. packetDec.data
	local checksumCorrect = bxor(calcSLCH(proto.icmp6, raw, phInfo, 3), 0xFFFF)
	raw = putU16(raw, 3, checksumCorrect)
	return raw
end

-- UDP --
decoders[proto.udp] = function (s, phInfo)
	assert(#s >= 8, "UDP packets have an 8-byte header")
	local checksum = getU16(s, 7)
	local checksumCorrect = bxor(calcSLCH(proto.udp, s, phInfo, 7), 0xFFFF)
	if checksumCorrect == 0 then
		checksumCorrect = 0xFFFF
	end
	if checksum ~= 0 then
		assert(checksum == checksumCorrect, "UDP checksum failure")
	end
	return {
		header = proto.udp,
		srcPort = getU16(s, 1),
		dstPort = getU16(s, 3),
		data = s:sub(9, getU16(s, 5))
	}
end

encoders[proto.udp] = function (packetDec, phInfo)
	local raw = genU16(packetDec.srcPort) .. genU16(packetDec.dstPort) .. genU16(#packetDec.data + 8) .. "\x00\x00" .. packetDec.data
	local checksumCorrect = bxor(calcSLCH(proto.udp, raw, phInfo, 7), 0xFFFF)
	if checksumCorrect == 0 then
		checksumCorrect = 0xFFFF
	end
	raw = putU16(raw, 7, checksumCorrect)
	return raw
end

-- TCP --
local tcpFlags = {
	"fin",
	"syn",
	"rst",
	"psh",
	"ack",
	"urg",
	-- the 6 extension flags
	"bottom",
	"up",
	"down",
	"strange",
	"charm",
	"top"
}

decoders[proto.tcp] = function (s, phInfo)
	assert(#s >= 20, "TCP packets have a 20-byte header")

	local checksum = getU16(s, 17)
	local checksumCorrect = bxor(calcSLCH(proto.tcp, s, phInfo, 17), 0xFFFF)
	assert(checksum == checksumCorrect, "TCP checksum failure")

	local flags = getU16(s, 13)
	local headerLen = math.floor(flags / 0x1000) * 4
	flags = band(flags, 0x0FFF)

	assert(headerLen >= 20, "TCP packets have to have a header length of >= 20 bytes")
	assert(#s >= headerLen, "TCP packet header must fit within TCP packet")

	return {
		header = proto.tcp,
		srcPort = getU16(s, 1),
		dstPort = getU16(s, 3),
		seqNum = getU32(s, 5),
		ackNum = getU32(s, 9),
		flags = decBits(flags, tcpFlags),
		windowSize = getU16(s, 15),
		-- checksum
		urgentPtr = getU16(s, 19),
		options = s:sub(21, 20 + headerLen),
		data = s:sub(headerLen + 1)
	}
end

encoders[proto.tcp] = function (packetDec, phInfo)
	local flags = encBits(packetDec.flags, tcpFlags)

	local options = packetDec.options or ""

	local hWords = math.floor((#options) / 4)
	assert((hWords * 4) == #options, "TCP options block size not multiple of 4")
	hWords = hWords + 5
	assert(hWords <= 15, "TCP options block size is too long")
	flags = flags + (hWords * 0x1000)

	local raw =
		genU16(packetDec.srcPort) .. genU16(packetDec.dstPort) ..
		genU32(packetDec.seqNum) ..
		genU32(packetDec.ackNum) ..
		genU16(flags) .. genU16(packetDec.windowSize) ..
		"\x00\x00" .. genU16(packetDec.urgentPtr or 0) ..
		options ..
		packetDec.data
	local checksumCorrect = bxor(calcSLCH(proto.tcp, raw, phInfo, 17), 0xFFFF)
	raw = putU16(raw, 17, checksumCorrect)
	return raw
end

return {
	getU16 = getU16,
	getU32 = getU32,
	genU16 = genU16,
	putU16 = putU16,
	genU32 = genU32,

	addOC16 = addOC16,
	addAreaOC16 = addAreaOC16,

	decPacket = decPacket,
	-- this bit of the API is unstable even by this project's standards r/n [
	decPacketDeep = decPacketDeep,
	decPacketDeepSafe = decPacketDeepSafe,
	-- ]
	encPacket = encPacket,

	findContentHeader = function (packet)
		-- This part is a bit complex, because it has to skip through headers *quickly*.
		local headerType = packet.header
		local data = packet.raw
		local headerPos = 1
		while true do
			if headerType == proto.ip6 then
				headerType = data:byte(headerPos + 6)
				headerPos = headerPos + 40
			elseif headerType == proto.ip6hbho or headerType == proto.ip6dsto or headerType == proto.ip6rout then
				-- Nice of them to make these bits basically identical.
				headerType = data:byte(headerPos)
				headerPos = headerPos + ((data:byte(headerPos + 1) + 1) * 8)
			else
				-- Content!
				break
			end
		end
		return headerType, headerPos
	end,

	calcSLCH = calcSLCH,

	proto = proto
}
