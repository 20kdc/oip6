--
-- oip6-driver-svc: IPv6 Driver Service Base / Deadport Daemon
--
-- Written starting in 2020.
--
-- To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
-- 

local data = require("oip6-data")
local filter = require("oip6i-driver-filter")

-- If using this, you still need to implement:
-- driver.getAddresses
-- driver.debug
-- driver.time
-- driver.at
-- BEFORE CALLING THIS!
-- Adds in the meat of the driver,
--  and returns a packet reception function.
return function (driver, ifaceTransmit)

	assert(type(driver.getAddresses) == "function", "getAddresses not implemented!")
	assert(type(driver.debug) == "function", "debug not implemented!")
	assert(type(driver.time) == "function", "time not implemented!")
	assert(type(driver.at) == "function", "at not implemented!")

	local listeners = {}

	local portFlags = {}

	driver.addListener = function (listener)
		listeners[listener] = true
		return listener
	end
	driver.rmListener = function (listener)
		listeners[listener] = nil
	end
	driver.lock = function (flag)
		assert(type(flag) == "string")
		local was = portFlags[flag]
		portFlags[flag] = true
		return was or false
	end
	driver.unlock = function (flag)
		assert(type(flag) == "string")
		portFlags[flag] = nil
	end
	driver.getLocks = function ()
		local locks = {}
		for k, _ in pairs(portFlags) do
			table.insert(locks, k)
		end
		return locks
	end

	local function svcReceive(packet, fromSelf)
		local encap = {
			header = data.proto.ip6,
			raw = packet
		}
		local hlType, hlPos = data.findContentHeader(encap)
		local ack = false
		for k, _ in pairs(listeners) do
			if k(packet, fromSelf, hlType, hlPos) then
				ack = true
			end
		end
		if not ack then
			-- The Deadport Daemon
			-- Ensure the packet actually parses as IPv6 before doing anything.
			-- This gets used as context for the sub-parsers.
			local ok, ip6 = pcall(data.decPacket, encap)
			if not ok then
				driver.debug("svc: unparsable head: " .. tostring(ip6))
				return
			end
			local multicast = ip6.dst:byte(1) == 255
			-- are we interested at all?
			if hlType ~= data.proto.udp and
			   hlType ~= data.proto.tcp and
			   hlType ~= data.proto.icmp6 then
				driver.debug("svc: don't care about " .. hlType)
				return -- no!
			end
			local ok, cont = pcall(data.decPacket, {
				header = hlType,
				raw = packet:sub(hlPos)
			}, ip6)
			if not ok then
				driver.debug("svc: unparsable innards: " .. tostring(cont))
				return
			end

			local sendMe

			if hlType == data.proto.icmp6 then
				-- ICMPv6 isn't handled normally.
				-- For example, multicast stuff has to be checked.
				-- In particular, be careful to prevent endless ICMP tennis...
				if cont.paramType == 128 and not multicast then
					-- echo request! need to send a reply
					sendMe = {
						header = data.proto.ip6,
						src = ip6.dst,
						dst = ip6.src,
						payload = {
							header = data.proto.icmp6,
							paramType = 129,
							paramCode = 0,
							data = cont.data
						}
					}
				elseif cont.paramType == 135 then
					-- Neighbour Solicitation
					local myG, myLL, myLP = driver.getAddresses()
					local intent = cont.data:sub(5, 20)
					if intent == myG or intent == myLL then
						-- Send a Neighbour Advertisement
						sendMe = {
							header = data.proto.ip6,
							src = intent,
							dst = ip6.src,
							payload = {
								header = data.proto.icmp6,
								paramType = 136,
								paramCode = 0,
								data = "\x40\x00\x00\x00" .. intent
							}
						}
					end
				end
			elseif hlType == data.proto.tcp and not multicast then
				-- terminate (sorry!)
				sendMe = {
					header = data.proto.ip6,
					src = ip6.dst,
					dst = ip6.src,
					payload = {
						header = data.proto.tcp,
						srcPort = cont.dstPort, dstPort = cont.srcPort,
						seqNum = (cont.ackNum + 1) % 0x100000000, ackNum = (cont.seqNum + 1) % 0x100000000,
						flags = {rst = true, ack = true},
						windowSize = 0, data = ""
					}
				}
			elseif hlType == data.proto.udp and not multicast then
				-- the port is unreachable
				sendMe = {
					header = data.proto.ip6,
					src = ip6.dst,
					dst = ip6.src,
					payload = {
						header = data.proto.icmp6,
						paramType = 1,
						paramCode = 4,
						-- yes, you send their packet back to them...
						data = "\x00\x00\x00\x00" .. packet
					}
				}
			end
			if sendMe then
				driver.transmit(data.encPacket(sendMe, sendMe).raw)
			end
		end
	end

	-- Filter installation
	local filterTransmit, filterReceive = filter(driver.getAddresses, ifaceTransmit, svcReceive, driver.debug)
	driver.transmit = filterTransmit
	return filterReceive
end
