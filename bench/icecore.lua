--
-- bench: testbench
--
-- Written starting in 2020.
--
-- To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
-- 

-- icecore test

local driver = ...

local addrLib = require("oip6-addr")
local iceCore = require("oip6i-ice-core")

local meG, meLl, meLp = driver.getAddresses()

local iceCore = iceCore(driver, {
	mdnsName = "bench.local",
	dns = "2606:4700:4700::1111"
}, function () end)

iceCore.dns("bench.local", function (addr)
	driver.debug("dns test says: " .. addrLib.stringify(addr))
end)

