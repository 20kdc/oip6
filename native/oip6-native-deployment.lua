--
-- oip6-native-deployment: installation reference for oip6
--
-- Written starting in 2020.
--
-- To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
-- 

local deployment = {
	openos = {
		{"common/oip6-addr.lua", "usr/lib/oip6-addr.lua"},
		{"common/oip6-data.lua", "usr/lib/oip6-data.lua"},
		{"common/oip6i-driver-filter.lua", "usr/lib/oip6i-driver-filter.lua"},
		{"common/oip6i-tcp.lua", "usr/lib/oip6i-tcp.lua"},
		{"common/oip6i-ice-core.lua", "usr/lib/oip6i-ice-core.lua"},
		{"common/oip6-driver-svc.lua", "usr/lib/oip6-driver-svc.lua"},
		{"common/oip6-ice-svc.lua", "usr/lib/oip6-ice-svc.lua"},
		{"game/unsandboxed/oip6.lua", "etc/rc.d/oip6.lua"}
	},
	psychos = {
		{"common/oip6-addr.lua", "lib/oip6-addr.lua"},
		{"common/oip6-data.lua", "lib/oip6-data.lua"},
		{"common/oip6i-driver-filter.lua", "lib/oip6i-driver-filter.lua"},
		{"common/oip6i-tcp.lua", "lib/oip6i-tcp.lua"},
		{"common/oip6i-ice-core.lua", "lib/oip6i-ice-core.lua"},
		{"common/oip6-driver-svc.lua", "lib/oip6-driver-svc.lua"},
		{"common/oip6-ice-svc.lua", "lib/oip6-ice-svc.lua"},
		{"game/unsandboxed/oip6.lua", "service/oip6.lua"}
	},
	kosneo = {
		{"common/oip6-addr.lua", "libs/oip6-addr.lua"},
		{"common/oip6-data.lua", "libs/oip6-data.lua"},
		{"common/oip6i-driver-filter.lua", "libs/oip6i-driver-filter.lua"},
		{"common/oip6i-tcp.lua", "libs/oip6i-tcp.lua"},
		{"common/oip6i-ice-core.lua", "libs/oip6i-ice-core.lua"},
		{"common/oip6-driver-svc.lua", "libs/oip6-driver-svc.lua"},
		{"common/oip6-ice-svc.lua", "libs/oip6-ice-svc.lua"},
		{"game/kosneo/svc-oip6.lua", "apps/svc-oip6.lua"}
	}
}

print("performing deployment...")

local cfg = require("oip6-native-config")
for ox, deployRules in pairs(deployment) do
	if cfg[ox] then
		for _, target in ipairs(cfg[ox]) do
			print(" to " .. ox .. " @ " .. target .. "...")
			for _, rule in ipairs(deployRules) do
				print("", rule[1], rule[2])
				os.execute("sudo -u " .. cfg.user .. " cp -rL ../" .. rule[1] .. " " .. target .. rule[2])
			end
		end
	end
end

