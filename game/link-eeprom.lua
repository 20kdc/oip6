-- Flat Network Rebroadcaster
local PORT = 1476
for u, _ in component.list("modem", true) do
	component.invoke(u, "open", PORT)
end
local tunnels = {}
for u, _ in component.list("tunnel", true) do
	tunnels[u] = true
end
while true do
	local tp, x, ta, tb, _, td = computer.pullSignal()
	if tp == "modem_message" and tb == PORT or tunnels[x] then
		for u, _ in component.list("modem", true) do
			if x ~= u then
				component.invoke(u, "broadcast", PORT, td)
			end
		end
		for u, _ in component.list("tunnel", true) do
			if x ~= u then
				component.invoke(u, "send", td)
			end
		end
	end
end
