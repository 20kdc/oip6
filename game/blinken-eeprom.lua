-- Works as an EEPROM or as an OpenOS program.
-- Primary use: A space filler.

local component = component or require("component")
local computer = computer or require("computer")
local event
if require then
	event = require("event")
end

local lb = component.proxy(component.list("light_board", true)())
local lbi = 0
local lbl = 0
local lbd = true
local lbt = computer.uptime()

local function update()
	local thisTime = computer.uptime()
	if thisTime > lbt then
		lbt = lbt + (1 / 8)
		lb.setColor(lbl + 1, 0x000000)
		lb.setColor(lbi + 1, 0x800000)
		lbl = lbi
		if lbd then
			lbi = lbi + 1
			if lbi == lb.light_count then
				lbi = lb.light_count - 1
				lbd = false
			end
		else
			lbi = lbi - 1
			if lbi == -1 then
				lbi = 0
				lbd = true
			end
		end
		lb.setActive(lbi + 1, true)
		lb.setColor(lbi + 1, 0xFF0000)
	end
end

if event then
	local function pd()
		update()
		local time = math.max(0.05, (lbt - computer.uptime()) + 0.001)
		event.timer(time, pd)
	end
	pd()
else
	while true do
		update()
		computer.pullSignal(0)
	end
end