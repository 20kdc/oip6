# Host-Driver Interface (oip6-driver-svc, oip6-ice-svc)

The Host-Driver Interface is the platform-specific component that hosts the OIP6 stack.

The OIP6 stack itself is split into two components:

The core driver (external interface `oip6-driver.md`)

The Internet Card Emulator which uses the resources the core driver provides (external interface `oip6-ice.md`)

The *internal* interface of these components is as follows...

## oip6-driver-svc

This takes `(driver, sendPacket)`, where these are:

`driver`: This is the skeleton driver to 'complete'. This must contain all of:

```
driver.getAddresses
driver.debug
driver.time
driver.at
```

before the instance can be safely created.

`sendPacket`: This is the function to send packets that gets wrapped to implement `driver.transmit`.

It completes the skeleton driver to create a complete driver, and returns an 'on packet receive' function.

## oip6-ice-svc

This takes `(driver, config, inetReady)`, where these are:

`driver`: A complete OIP6 Driver.

`config`: The configuration for the OIP6 Driver.

`inetReady`: A function `(connectionId)` which is called to perform `computer.pushSignal("internet_ready", uuid, connectionId)`.

It returns `instance, close`, where these are:

`instance`: The completed component for immediate use.

`close`: A function to 'close' the ICE (cleaning up, for example, port locks).

