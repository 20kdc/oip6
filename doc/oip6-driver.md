OIPv6 Simple Node Driver Specification
---------------------------------------

# Disclaimer

This is intended for use in a game. The implementation of IPv6 should ideally be compliant for situations where the game is directly bridged with the real world, but this is not guaranteed and it is not the place of this specification regardless. Still, if there are any holdings on the use of the name "IPv6", please be aware that this IS intended as part of an IPv6 implementation.

# Abstract

The OIPv6 Simple Node Driver is designed for OpenComputers.

OpenComputers has relatively stringent RAM requirements when run with Lua 5.3 x86_64, and attempting to add routing to this specification
 made it a complete and total disaster to actually hope to ever use.

As such, this driver covers the practical situation of a single node address and broadcast-to-everything routing.

Hop limits are respected by never forwarding anything ever, as that is the job of a router.

Essentially, by treating OC networks as what they are; a link layer which has absolutely no cross-talk or corruption but is for some reason incredibly slow; an IPv6 network can be maintained.

Any complications for larger networks are intended to be handled using server racks and modifying the OpenComputers configuration to make networking faster.

Do be aware: a router that also implements the OIPv6 Simple Node Driver specification is perfectly possible.

# Acquisition

There can only be one OIPv6 Simple Node Driver per system.

Where it is running is implementation-dependent, but it MUST NOT be setup in a 'first application runs the driver' manner or similar.

(Simply put, the driver must support multiple applications that start/stop independently.)

It's perfectly fine for a "driver service" application to host the driver, however, as this application is dedicated for the purpose.

The acquisition of the library that controls it is partially OS-dependent.

For OpenOS, it must be through `require("oip6-driver")`.

For KittenOS NEO, it must be through the permission `x.svc.oip6`.

In both cases the purpose is to ensure enough control to avoid driver operation being in conflict with application operation.

For operating systems not specified here:

"OpenOS-style" non-sandboxed systems (this may include PsychOS) should use the OpenOS mechanism.

"KittenOS NEO-style" sandboxed systems should use whatever mechanism is most convenient while maintaining a required permission to access IPv6 features.

# Methods

## driver.getAddresses()

Returns three 16-byte strings:

The global address, which is the IPv6 global ('-ish' if under NAT) address of this computer's external interface.
(NOTE: If no global address exists, then this should be replaced with the link-local address.)

The link-local address.

The loopback address (a constant).

The driver may change these addresses during runtime if specifically forced to by user or API, under the understanding that running network applications may or may not adapt to the change.

## driver.setGlobalAddress(address)

NOTE: This function is optional. In particular, it may not exist on routers, where router configuration is intertwined.

Updates the 16-byte global address.
The driver should verify that it's a 16-byte string.

## driver.transmit(packet)

Transmits an IPv6 packet.
If the packet's destination is loopback, it isn't transmitted at all and is received on the packet listeners.

## driver.addListener(listener)

Adds a packet listener (prototype `ack = listener(packet, fromSelf, hlType, hlPos)`). Returns the passed listener function for passing to rmListener.

`packet` is the raw packet data, `fromSelf` is true on failure, `hlType` and `hlPos` mark the header type and location of the high-level protocol.

(This is as defined by `oip6-data.lua`'s `findContentHeader` function; the IPv6 header itself, destination options, hop-by-hop options, and routing header are not eligible; everything else is. However, fragments will never arrive here unless preceded by unsupported headers.)

If the packet listener itself returns something truthy, the packet is considered handled, so it will not be sent to the Deadport Daemon.

Note that all packet listeners still receive the packet, and the handling is OR'd.

## driver.rmListener(listener)

Removes a packet listener. This can be automatic on operating systems where's it's possible to automate, but not on others.

## driver.lock(flag)

Given a string for the name of the mutex to lock, locks that mutex.

This is useful to prevent any chance of conflicts between temporary ports.

The lock table also acts as a way to check the active connections of a computer.

For TCP ports, the format "tcp.<port>" is used, such as "tcp.80".

For UDP ports, the format "udp.<port>" is used.

Returns false on failure, true on success.

## driver.unlock(flag)

Given a string for the name of the mutex to unlock, unlocks that mutex.

This MAY only be performable by the process that locked the mutex in the first place.

## driver.getLocks()

Returns an ipairsable table of all locked names.

## driver.time()

This is an operating-system-independent 'get time' function.

The unit is seconds, but it may have any resolution and may have any.

It is intended for use by timeouts and other timing business that needs to be usable by IPv6 stack libraries that can be used from within a testbench.

## driver.at(time, fn)

At a given time (as determined by `driver.time()`), calls the given function.

## driver.debug(text)

Prints debug text somewhere (in case stdio are used for something)

# Routing

## A quick note on address terminology

Multicast addresses are defined here to be addresses that have a first byte of 255.

Interface-local addresses are the loopback address and all interface-local multicast addresses (`ffx1::`)

SelfAddr addresses are all 3 getAddresses results.

## A Note On The Interface

The interface the driver sends/receives packets from isn't defined here, but it must be a single logical interface.

For example, implementing the driver connected to the 'internal' arm of a router to provide remote configuration services is acceptable.

As such, outside of NAT or other complex situations (that standard applications may not handle well anyway without being forced onto one 'preferred' side, in which case this specification may be applied again), the specification should be a reasonable compromise.

## Overview

```
digraph Routing {
 compound=true;

 subgraph local {
  node [style="filled"];

  "In" -> "SVC";
  "SVC" -> "(user programs)" [label="packet listeners"];
  "(user programs)" -> "Out" [label="driver.transmit()"];

  "SVC" -> "Deadport Daemon" [label="if unhandled"];
  "Deadport Daemon" -> "Out" [label="driver.transmit()"];
 }

 subgraph filter {
  node [style="filled", fillcolor=black, fontcolor=white];

  "Fragment Reassembly" -> "In" [label="After assembly,\n or for unfragmented packets"];
  "Receive Filter" -> "Fragment Reassembly" [label="Src isn't SelfAddr/Multicast\n& Dst isn't Interface-local\n& Dst is SelfAddr/Multicast"];

  "Send Filter" -> "Fragment Reassembly" [label="Dst is SelfAddr/Multicast\n& Src isn't Multicast"];
  "Out" -> "Send Filter";
 }

 // --


 "Reception" -> "Receive Filter";
 "Send Filter" -> "Transmission" [label="Dst isn't SelfAddr or Interface-local"];

}
```

NOTE: In the reference implementation, the receive filter, send filter, and fragment reassembly are all in one library (inverted).

The Deadport Daemon and other components behind the filter are in a separate library (shaded).

("(user programs)" is a set of callbacks that are registered with SVC, so isn't strictly part of SVC, but is at that 'location' routing-wise.)

oip6-driver-svc implements the packet listener interface and the fallthrough to the Deadport Daemon, along with that daemon.

## Receive Filter

The driver must discard packets if:
1. their source address is SelfAddr (as these are clearly fake or duplicated)
2. their source address is multicast (because that makes no sense)
3. their destination address is interface-local

In addition, if the destination address is not SelfAddr or multicast, the packet must be discarded.

All packets that pass this must be passed to Fragment Reassembly.

## Send Filter

Output is duplicated between two pipelines, Local and Remote.

### Local

The driver must discard packets if:
1. their source address is multicast (because that makes no sense)
2. their destination address is not SelfAddr or multicast.

All packets that pass this must be passed to Fragment Reassembly.

### Remote

The driver must discard packets if:
1. their destination address is SelfAddr.
2. their destination address is interface-local.

All packets that pass this must be sent to the interface as-is.

## Fragment Reassembly

Fragment Reassembly is the last stage before the packet listeners.

Fragment Reassembly must be capable of skipping Hop-By-Hop Options, Destination Options, and Routing extension headers.

All packets that are successfully reassembled here or arrive here unfragmented go to SVC.

Reassembly of fragmented packets must be a component of the driver to at least the specification required by IPv6 (1500 bytes) and ideally exceeding it.

## SVC

SVC sends packets to the packet listeners. If none handle the packets, then it sends them to the Deadport Daemon (if present).

## Deadport Daemon

The Deadport Daemon is a useful *optional* task, placed in the driver to avoid an additional service, that implements key 'global' elements of ICMPv6, UDP, and TCP in the core.

As it's optional, all of this is arguably advisory.

It is only used for unhandled packets, and is meant to cover singleton tasks like non-listening-port connection refusal, or ICMPv6.

Specifically:

1. It closes any TCP connection it receives (on the basis that if it was known, it would be handled.)

2. It sends Destination Unreachable packets for unacknowledged UDP packets.

3. It implements ICMPv6 Ping responding.

4. It implements ICMPv6 Neighbour Solicitation responding.
NOTE: NOT IMPLEMENTING AT LEAST THIS COMPONENT *WILL BACKFIRE*.

Note that the optional nature of this implies that it is allowed to, for example, temporarily stop functioning if the driver is overwhelmed with packets. This is important when a driver's listener calls are decoupled from the actual execution thread, so a timeout has to be used to decide when a packet hasn't been responded to.

# IPv6 Features That May Be Ignored

1. Hop-By-Hop Options, Destination Options, and Routing extension headers must be supported insofar as encountering these extension headers when they have no effect will not cause any issues.
They must not be removed.

It is up to applications to work out what to do with these headers, though it's likely applications will ignore them too...

2. The *creation* of fragmentation is the responsibility of the program, and thus packets will likely stay bounded to 1280 bytes.
If in future it's decided packets should exceed 1280 bytes, this will be done using a 'virtual path MTU' with fragmentation created at the bridge.

3. The ICMPv6 messages relating to a header type being unknown will not be generated for now.

4. The TCP Urgent Data system is in a state where one cannot possibly win, and the "OOB" convention breaks the idea of TCP being networked pipes.
Pretending it doesn't exist properly implements the IETF OOBINLINE requirement, though it makes the TCP implementation theoretically incomplete.
For further information, please see RFC 6093.

# OpenComputers Link Layer Specification

The unofficial (by both OC and IPv6 standards) embedding of IPv6 over the OpenComputers link layer works as follows:

The driver sends all packets it wishes to transmit, as-is, as a single binary string value, on port 1476.

In implementations not specifically designed for routing, the driver must ignore all "MAC Address"-style link-layer business.

It's more trouble than it's worth on OpenComputers, where transmission speed is the bottleneck, not reception speed or transmission breadth.
The medium has infinite bandwidth, the receivers have infinite bandwidth, but the transmitters don't.)

(For example, the necessity of a 'gateway' setting and a prefix length on a computer's address is due to link-layer business. If broadcasting, all gateways will always receive, and be able to, if required, route any packet.)


