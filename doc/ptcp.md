%%% title = "Practical TCP" %%%

# Introduction

The Transmission Control Protocol [@-tcp] is the basis of most modern Internet
communication, second only to IP itself.

However, it is based on a complex series of actions available to packets, while
also being based on a complex state machine.

This specification re-expresses the state machine as part of the actions that
packets can perform.

Additionally, it simplifies areas that have become outdated in the past 30 years
since the last update/clarification of the specification.

This memo defines a non-strict subset of TCP that should be compatible with
modern implementations while reducing the complexity and making no extensions.

# Terminology

A 'peer' is either side of the TCP connection.

The 'local peer' and 'remote peer' are used relative to each other.

They are NOT 'server/client'.

The 'client' is the sender of a standard connection request.

The 'server' is the recipient of a standard connection request.

# Header Format

This document defines only a set of semantics.

As such, the header format remains the same as non-subsetted TCP.

# Routing From Host Drivers To Connections

The routing of an incoming packet from a host IP driver to a specific instance
 of the connection SHOULD be done with the order of priorities given here in
 most systems:

1. Is there any connection with this exact source and destination address/port?

2. Is there anything willing to accept a new connection for the destination?

3. Send an (RST,ACK) packet, with the sequence number being equal to the
incoming packet acknowledgement number +1, and the acknowledgement number being
equal to the incoming packet sequence number +1.

# Overview Of Operation

## Stream Model

TCP is used as an ordered byte stream.

The PSH flag theoretically could emulate an ordered list of byte buffers, but
this isn't usable in practice as the APIs in modern systems do not allow a proxy
to properly replicate this effect.

This implies that PSH SHOULD be set on any packet that contains data, for
safety with systems that do support PSH, and SHOULD NOT be set otherwise, as
there's no point.

If not performing this, then a peer MUST attempt to implement PSH according to
the standard set in [-rfih].

## Actions

There are three valid actions: SYN, FIN, and individual bytes of data.

(That is, each byte of data is a single action.)

SYN and FIN are the corresponding flags, while the bytes of data are as
provided in the data area of the packet.

Sequence numbers are placed a ring of actions, modulo 0x100000000.

Each action takes up a single slot in the ring.

The sequence number of a packet (as opposed to the acknowledgement number)
indicates where on the ring the actions of a packet are located.

Each packet maps to a list of contiguous actions on the ring.

They are put into the ring in this order: SYN, data, FIN.

SYN can only be received validly once, when the local peer has no
acknowledgement number available, as it sets the acknowledgement number of the
remote peer.
If received at any other time, it is treated as a NOP (but is still an action).

Data is bytes of the byte stream being transmitted over TCP.

FIN is an action that, when received, indicates the remote peer has stopped
creating data and will not send any future actions.

The remote peer SHOULD NOT send any future actions after signalling FIN.
If it does so, the local peer MAY discard the data actions.

The remote peer MAY send previous actions including the FIN (as re-transmits).

## Sending Data & Receiving Acknowledgement

Each peer maintains a metaphorical 'buffer of actions'.

These are the actions that the local peer needs to send to the remote peer.

This buffer is metaphorical; it only has to implement the effect of ensuring
that if a sent action is lost, it can be re-sent.

Sending-wise, there are various strategies for implementing the action buffer
and its relation to packets.

A local peer MAY forget any actions that have been acknowledged, i.e. become
unable to retransmit them should the remote peer's acknowledgement number
'go backwards'.

The assumption that should be made in regards to receiving acknowledgement if
the remote peer's acknowledgement number does in fact go backwards is that this
is a re-ordered packet; acknowledgement-wise, it should be ignored.

A local peer MUST NOT forget any actions that have not been acknowledged by the
remote peer.

A local peer that re-sends the same packet until completion MAY treat an ACK
from the remote peer that confirms receipt but not completely for a packet as if
it didn't confirm receipt at all.

(This is fine, because when receiving data the remote peer must accept overlap.
So long as the current packet is being re-sent, when the remote peer eventually
reads the remainder of the actions, the remote peer will then send a completed
ACK.)

## Receiving Data & Sending Acknowledgement

The local peer has an acknowledgement number; this is better described as a
'next action that will be executed' number.

It watches packets to see if they contain any action at that number, and if so
applies that action, advances the acknowledgement number, and checks for the
next action in any packets it has (including the current one), continuing until
it has completed all actions available to it.

To clarify: The local peer MUST accept situations where it's acknowledgement
number is half-way in a received packet, and execute the actions.

(Otherwise, it is possible for simpler implementations to become stuck.)

A peer MAY store packets that it believes to be 'from the future' for later
combining into the stream. It MAY instead ignore them.

A peer SHOULD always include ACK in packets after it has an acknowledgement
number, unless it is somehow absolutely certain that the current state has been
received. Enabling ACK on unrelated packets costs nothing.

A peer MUST ensure any packet it sends with ACK is updated to the latest
acknowledgement number at the time of sending.

A peer MUST send at least (but not limited to) an ACK in response to:

1. Any packet that causes the execution of actions.
2. Any packet that has a sequence number not equal to the local peer's
acknowledgement number.

(For the reasoning, see "TCP Keep-Alives" [-rfih].)

## Connection Logic

A peer MUST start a connection with a SYN effectively in the buffer.

It can have any sequence number.

A server SHOULD optimize the inbound connection by acquiring the incoming SYN
and applying the acknowledgement number before sending the outbound SYN.

As stated previously, only the first SYN 'counts'; as SYN ignores the present
sequence number, a duplicate SYN would otherwise be able to damage the
connection.

Upon receiving an RST, a peer SHOULD treat the connection as dead immediately,
no matter what state it is in.

Meanwhile, if a FIN is received, the local peer SHOULD add a FIN after all
pending data.

In this state, the connection is 'closed', but not dead.

It's useful to only do this after the application has had an opportunity to send
all data that it wants to send.

The connection SHOULD not be considered dead in this case until after the
sent FIN has been acknowledged.

## The Window

If Window Size isn't a concern (for Node.js-style data-event APIs), it SHOULD be
set to 65535.

If the implementation only sends one packet at a time and waits for
the retransmission timer or an acknowledgement before sending another packet,
it MAY completely ignore received Window Size.

This is because the retransmissions themselves implement zero-window-probing.

Otherwise, when insufficient window is available, the implementation MUST limit
itself to only sending the first packet of the data it wishes to send, as if it
was one of the former implementations.

# Simpler Packeting And Congestion Evasion

An implementation aiming for simplicity likely does not wish to implement the
rather large set of algorithms that make up modern TCP.

As such, a 'lockstep' algorithm ala TFTP [-tftp] is described here.

In this algorithm:

The local peer only ever tries to send one packet's worth of actions to the
remote peer.

It only advances to the next packet's worth of actions after the present one
has been completed.

As such, Window Size is a non-concern as described previously.

Upon a request to send data to the remote peer, the local peer would add the
data to a buffer.

If this buffer is finite, the local peer waits until room is available.

The local peer only stores the current packet being sent (implicitly including
the actions within), discarding it once that packet has been fully acknowledged.

At any time where the state of the local peer is updated and there is no current
packet being sent, the local peer tries to:

1. Create and then send a data packet, with all available data.
2. If there is no available data, check to see if a FIN packet should be sent,
and if so, send it.

Regarding congestion control, retransmissions occur at 1, 2, 4 & so on seconds.

Do be aware: The 'wait for acknowledgement before sending a new packet' part
implicitly implements the Nagle Algorithm.

According to [-rfih], applications are supposed to be allowed to turn this off.

However, in implementations opting for simplicity, this is not ideal.

As the Nagle Algorithm or lack thereof doesn't affect protocol compliance from
the perspective of the remote peer, this is arguably just an efficiency matter.

Still, one method of allowing this without redesigning the sending component,
is to have the send function directly send additional packets based on the
current sequence number and the size of the transmit buffer, and then add the
data into the transmit buffer.

The idea behind this is that appending these packets onto the 'finished'
sequence number after the execution of the current packet being retransmitted
is valid so long as no other actions get inserted in the middle.

As there is no reason to send a SYN at that time, and as FINs should only be
sent after all transmit buffer data has been consumed, that will not happen.

# Unhandled Parts Of TCP

The Urgent Pointer cannot be considered a stable feature at this point.

As such, this document recommends the 'total and complete ignorance' strategy;
by doing so, received data ends up in-line as per specifications, and sent data
is never urgent and thus can never fall foul of system-specifics.

Options are not described in this document.

With the exception of Max Segment Size, the minimum set of options do nothing.

# Credits

20kdc <gamemanj@hotmail.co.uk>

The original TCP RFC [-tcp], the later clarifications [-rfih],
the TUN/TAP interface (Linux flavour), Wireshark, a driving motivation to write
an IPv6 stack in Lua, and some luck, was what allowed me to create and test a
TCP implementation following this subset.

If possible by law, this memo is released into the public domain.
Otherwise, distribution of this memo is unlimited.

# Security Considerations

Urgent data is, in modern implementations, out-of-band by default, but the RFC
states that it is NOT out-of-band, and by not handling it, this subset follows
the RFC behavior by default.

While this is an aspect in which the subset is more RFC-compliant than modern
implementations while reducing complexity, the difference has previously caused
security issues - see the security considerations of [@-urg] for details.

The following of these instructions may have unknown effects on security under
situations that the original TCP RFC [@-tcp] and it's updates handle well.

On the other hand, the reduction in explaination complexity is likely to remove
opportunities for security-affecting bugs to enter the code in the first place.

# References

{backmatter}

<reference anchor='tcp' target=''>
 <front>
 <title>Transmission Control Protocol</title>
  <date year="1981" month="September"/>
 </front>
 <seriesInfo name="RFC" value="793" />
</reference>

<reference anchor='urg' target=''>
 <front>
 <title>On the Implementation of the TCP Urgent Mechanism</title>
  <date year="2011" month="January"/>
 </front>
 <seriesInfo name="RFC" value="6093" />
</reference>

<reference anchor='rfih' target=''>
 <front>
 <title>Requirements for Internet Hosts -- Communication Layers</title>
  <date year="1989" month="October"/>
 </front>
 <seriesInfo name="RFC" value="1122" />
</reference>

<reference anchor='tftp' target=''>
 <front>
 <title>THE TFTP PROTOCOL (REVISION 2)</title>
  <date year="1992" month="July"/>
 </front>
 <seriesInfo name="RFC" value="1350" />
</reference>

