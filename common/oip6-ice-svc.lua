--
-- oip6-ice: Simplified "Internet Card-style" API
--
-- Written starting in 2020.
--
-- To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
--

local dataLib = require("oip6-data")
local addrLib = require("oip6-addr")
local iceCoreLib = require("oip6i-ice-core")

local NAT64 = "\x00\x64\xff\x9b\x00\x00\x00\x00\x00\x00\x00\x00"

-- returns instance, close
return function (driver, config, inetReady)
	local iceCore = iceCoreLib(driver, config, inetReady)

	local function immediateAddressParse(address)
		if address == "localhost" then
			local _, _, loopback = driver.getAddresses()
			return loopback
		end
		local ok, res = pcall(addrLib.parseV4, address)
		if ok then
			return NAT64 .. res
		end
		local ok, res = pcall(addrLib.parse, address)
		if ok then
			return res
		end
		-- Oh dear.
	end

	local function verifyPort(port)
		return math.floor(port) == port and port >= 0 and port <= 65535
	end

	local function getHostByName(address, finishedCB)
		local immediate = immediateAddressParse(address)
		if immediate then
			finishedCB({immediate})
		else
			iceCore.dns(address, finishedCB)
		end
	end

	local function connectTCP(address, port)
		if not port then
			-- NOT a proper URL parser, but still welding bits together here
			local lhs, rhs
			if address:sub(1, 1) == "[" then
				-- [IPv6]:port
				local ender = address:find("]:", 1, true)
				if not ender then
					return nil, "unterminated or unportified IPv6 address in URL"
				end
				lhs, rhs = address:sub(2, ender - 1), address:sub(ender + 2)
			else
				-- domain:port or ipv4:port
				local ender = address:find(":", 1, true)
				if not ender then
					return nil, "no port in URL"
				end
				lhs, rhs = address:sub(1, ender - 1), address:sub(ender + 1)
			end
			address = lhs
			port = tonumber(rhs)
			if (not port) or (not verifyPort(port)) then
				return nil, "port invalid"
			end
		end
		local immediate = immediateAddressParse(address)
		local gameId = iceCore.newGameId()
		local proxy, proxyShift = iceCore.tcpProxy(gameId)
		getHostByName(address, function (responses)
			if #responses == 0 then
				proxyShift(nil, "failed to locate address")
			else
				local addr = responses[math.random(#responses)]
				if addr:byte(1) == 255 then
					proxyShift(nil, "multicast address cannot be connected to")
				else
					proxyShift(iceCore.tcp(gameId, addr, port))
				end
			end
		end)
		return proxy
	end

	return {
		-- TCP
		isTcpEnabled = function ()
			return true
		end,
		connect = connectTCP,
		listen = function (port, address)
			if not verifyPort(port) then
				return nil, "port invalid"
			end
			if address ~= nil then
				local ok, a = pcall(addrLib.parse, address)
				if not ok then
					return nil, "address invalid"
				end
				address = a
				if address:byte(1) == 255 then
					return nil, "multicast address cannot be listened on"
				end
			else
				address = addrLib.unspecified
			end
			return iceCore.tcpListener(address, port)
		end,
		-- HTTP
		isHttpEnabled = function ()
			return false
		end,
		request = function ()
			return nil, "NYI"
		end,
		-- Other
		hostAddress = function ()
			local address, _, _ = driver.getAddresses()
			return addrLib.stringify(address)
		end,
		linkLocalAddress = function ()
			local _, address, _ = driver.getAddresses()
			return addrLib.stringify(address)
		end
	}, iceCore.close
end
