--
-- bench: testbench
--
-- Written starting in 2020.
--
-- To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
-- 

local args = {...}

package.path = package.path .. ";../common/?.lua"

local driver = require("oip6-driver")

loadfile(args[1])(driver)

driver.beginTest()

