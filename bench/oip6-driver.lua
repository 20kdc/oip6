--
-- oip6-driver: Reference outside-of-OC implementation for testbench use
--
-- Written starting in 2020.
--
-- To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
-- 

-- Reference Outside-Of-OC Implementation Of The OIP6 Driver
-- ideally your own implementation would be more flexible than this
--  for the global and link-local addresses

local data = require("oip6-data")
local addr = require("oip6-addr")

local IDLE_ALERT = false

local myGlobalAddress = addr.parse("fda5:b1f9:42ba:4e57:0054:0000:1214:1214")
local myLinkLoAddress = addr.parse("fe80:0000:0000:0000:9fef:b7cd:1214:1214")
local loopbackAddress = addr.parse("::1")

local driver = {}

local timers = {}

-- OS abstraction stuff
driver.time = os.time
driver.at = function (at, fn)
	timers[{at, fn}] = true
end
driver.getAddresses = function ()
	return myGlobalAddress, myLinkLoAddress, loopbackAddress
end
--

function driver.debug(text)
	io.stderr:write(text .. "\n")
	io.stderr:flush()
end

-- Implements platform-independent 'singleton' bits of the API,
--  in particular the innards of the packet listener framework & the Deadport Daemon
local filterReceive = require("oip6-driver-svc")(driver, function (packet)
	-- real transmission
	local n = (#packet) + 4
	local hdr = ""
	n, hdr = math.floor(n / 256), string.char(n % 256) .. hdr
	n, hdr = math.floor(n / 256), string.char(n % 256) .. hdr
	n, hdr = math.floor(n / 256), string.char(n % 256) .. hdr
	n, hdr = math.floor(n / 256), string.char(n % 256) .. hdr
	io.write(hdr .. packet)
	io.flush()
	driver.debug("sent packet [" .. #packet .. "]")
end)

-- Internal
local function updateTimers()
	local timersToRun = {}
	for k, v in pairs(timers) do
		if os.time() >= k[1] then
			table.insert(timersToRun, k)
		end
	end
	for _, v in ipairs(timersToRun) do
		timers[v] = nil
		v[2]()
	end
end

driver.beginTest = function ()
	local buf = ""
	while true do
		local chunk = io.read(1)
		if not chunk then return end
		updateTimers()
		buf = buf .. chunk
		while #buf >= 4 do
			local n = 0
			n = n + (buf:byte(1) * 0x1000000)
			n = n + (buf:byte(2) * 0x10000)
			n = n + (buf:byte(3) * 0x100)
			n = n + (buf:byte(4) * 0x1)
			if #buf >= n then
				local packet = buf:sub(5, n)
				if packet ~= "" then
					driver.debug("got packet [" .. #packet .. "]")
					filterReceive(packet)
				elseif IDLE_ALERT then
					driver.debug("got idle")
				end
				buf = buf:sub(n + 1)
			else
				break
			end
		end
	end
end

return driver
