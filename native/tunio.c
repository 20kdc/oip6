/*
 * TUNio
 *
 * Written starting in 2019.
 *
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 * 
 * In lieu of a proper repository, please sign your preferred name, the date, and summary of changes here.
 *
 * 20kdc - December 1st 2019 - Initially created TUNio
 * 20kdc - March 16th 2020 - Allow creating 'null timing packets' to wake up directly connected Lua scripts
 * 20kdc - March 23rd 2020 - Performance improvements
 * 
 */

#include <arpa/inet.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <linux/if_arp.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define MAX_PACKET_SIZE 65536
#define MAX_EXT_PACKET_SIZE (MAX_PACKET_SIZE + sizeof(uint32_t))

int opt_link = -1;
int opt_wakeup = 0;

// should start at the first digit. returns *after* the last digit.
int decode_integer_option(char ** opts) {
	int value = 0;
	while (1) {
		char ch = **opts;
		if ((ch >= '0') && (ch <= '9')) {
			value *= 10;
			value += ch - '0';
			(*opts)++;
		} else {
			// this includes the null terminator
			break;
		}
	}
	return value;
}

int decode_options(char * opts, struct ifreq * tunreq) {
	if (opts[0] == 'U') {
		tunreq->ifr_flags = IFF_TUN;
	} else if (opts[0] == 'A') {
		tunreq->ifr_flags = IFF_TAP;
	} else {
		fputs("first opt char. can be tUn or tAp\n", stderr);
		return 1;
	}
	opts++;
	while (*opts) {
		if (opts[0] == 'N') {
			tunreq->ifr_flags |= IFF_NO_PI;
		} else if (opts[0] == 'T') {
			opts++;
			opt_link = decode_integer_option(&opts);
			// because of the later increment
			opts--;
		} else if (opts[0] == 'W') {
			opt_wakeup = 1;
		} else {
			fputs("unable to understand flag\n", stderr);
			return 1;
		}
		opts++;
	}
}

int main(int argc, char ** argv) {
	if (argc != 3) {
		fputs("./tunio <name> <U|A>[N][T<int>]\n", stderr);
		fputs(" generates a TUN for interfacing with applications unable to make IOCTLs\n", stderr);
		fputs(" in combination with a TCP connection, makes a tunnel\n", stderr);
		fputs(" please note: this adds a big-endian uint32_t 'packet length' at the start of every packet\n", stderr);
		fputs(" the packet length measurement includes itself to prevent an overflow\n", stderr);
		fputs(" that said, obviously, be careful when passing around TUNs to arbitrary sources\n", stderr);
		fputs(" as misconfiguration == a bypass of your firewall, and nobody likes that, do they\n", stderr);
		fputs(" arg 1: name\n", stderr);
		fputs(" arg 2: options...\n", stderr);
		fputs("  decimal integers are terminated with non-digit or end of arg\n", stderr);
		fputs("  U: IFF_TUN\n", stderr);
		fputs("  A: IFF_TAP\n", stderr);
		fputs("  N: IFF_NO_PI\n", stderr);
		fputs("  T: TUNSETLINK (decimal integer); note that this CHANGES link type\n", stderr);
		fputs("      which may be important if you have persistent taps around.\n", stderr);
		fputs("     usually you want T1 (aka ARPHRD_ETHER), but see /usr/include/linux/if_arp.h\n", stderr);
		fputs("  W: sends empty packets every idle second to 'wake up' a program using blocking I/O\n", stderr);
		fputs(" example 1 (dumping):\n", stderr);
		fputs("  ./tunio idte AT1 | hexdump -C\n", stderr);
		fputs("  ip link set idte up\n", stderr);
		fputs("  ping fe80::0010%idte\n", stderr);
		fputs(" example 2 ('echo') - useful for testing:\n", stderr);
		fputs("  mkfifo mfifo\n", stderr);
		fputs("  cat mfifo | ./tunio alice AT1 | ./tunio bob AT1 > mfifo\n", stderr);
		fputs("  ip link set alice up\n", stderr);
		fputs("  ip link set bob up\n", stderr);
		fputs("  # use a packet capturer (wireshark perhaps) to watch the fun\n", stderr);
		fputs("  # every packet should be duplicated, without exception\n", stderr);
		return 1;
	}

	int tun = open("/dev/net/tun", O_RDWR);
	if (tun < 0) {
		fputs("Failed to open /dev/net/tun\n", stderr);
		return 1;
	}

	// Credit to Linux Documentation/networking/tuntap.txt
	// But no help from them in regards to explaining how packets are framed purely by read/write return values (whaaa)
	struct ifreq tunreq;
	memset(&tunreq, 0, sizeof(tunreq));
	if (decode_options(argv[2], &tunreq)) {
		fputs("Bad options\n", stderr);
		return 1;
	}
	strncpy(tunreq.ifr_name, argv[1], IFNAMSIZ);
	if (ioctl(tun, TUNSETIFF, &tunreq) < 0) {
		fputs("Failed to setup tun\n", stderr);
		return 1;
	}
	if (opt_link != -1) {
		if (ioctl(tun, TUNSETLINK, opt_link) < 0) {
			fputs("Failed to set link type\n", stderr);
			return 1;
		}
	}

	// Transmission buffer has a 'state machine'
	char tx_buffer[MAX_EXT_PACKET_SIZE];
	// Position in transmission buffer.
	uint32_t tx_buffer_pos = 0;
	// Amount of data to read.
	uint32_t tx_buffer_size = 4;
	
	while (1) {
		fd_set read_set;
		FD_ZERO(&read_set);
		FD_SET(0, &read_set);
		FD_SET(tun, &read_set);
		struct timeval timeout = { .tv_sec = 1 };
		// nfds logic:
		// since tun cannot be under 0, it must thus be highest by default
		// send_wakeup logic:
		// if we're being woken up anyway, we don't really need to worry about
		// sending an idle packet to wake anything up
		int send_wakeup = 0;
		if (select(tun + 1, &read_set, NULL, NULL, &timeout) == 0)
			send_wakeup = opt_wakeup;
		if (FD_ISSET(0, &read_set)) {
			// sending packet into TUN
			// this packet may get fragmented in all sorts of fun ways
			// hence, we use the big-endian 4-byte header, but this adds complexity
			ssize_t rd = read(0, tx_buffer + tx_buffer_pos, tx_buffer_size - tx_buffer_pos);
			if (rd > 0) {
				// Has to be castable since length requested had to be castable and it's > 0.
				tx_buffer_pos += (uint32_t) rd;
				if (tx_buffer_pos == tx_buffer_size) {
					// Update buffer size
					tx_buffer_size = ntohl(*((uint32_t*) tx_buffer));
					// Important: CHECK THE SIZE!
					// Not doing this is the path to buffer overflows.
					if (tx_buffer_size < 4) {
						fputs("User sent len < 4 (invalid)\n", stderr);
						return 1;
					} else if (tx_buffer_size > MAX_EXT_PACKET_SIZE) {
						fputs("User sent len > max_ext_packet_size (would overflow)\n", stderr);
						return 1;
					} else if (tx_buffer_pos == tx_buffer_size) {
						// It has ended!
						// Transmit the packet!
						if (tx_buffer_size != 4)
							write(tun, tx_buffer + 4, tx_buffer_size - 4);
						// Reset state machine!
						tx_buffer_pos = 0;
						tx_buffer_size = 4;
					}
				}
			}
		}
		if (FD_ISSET(tun, &read_set)) {
			// receiving packet from TUN, this just needs an rx buffer
			char rx_buffer[MAX_PACKET_SIZE];
			ssize_t res = read(tun, rx_buffer, MAX_PACKET_SIZE);
			if (res > 0) {
				uint32_t temp = (uint32_t) res;
				// Make sure this includes the length itself
				temp += 4;
				temp = htonl(temp);
				// preface header
				write(1, &temp, sizeof(uint32_t));
				// packet content
				write(1, rx_buffer, res);
				send_wakeup = 0;
			}
		}
		if (send_wakeup) {
			uint32_t temp = htonl(4);
			write(1, &temp, sizeof(uint32_t));
		}
	}
	// not that it should ever get here...
	close(tun);
}
