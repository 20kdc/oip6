--
-- oip6-addr: An attempt to implement RFC 4291 & RFC 5952 addressing rules.
--
-- Written starting in 2020.
--
-- To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
--

local band

if bit32 then
	-- Lua 5.2
	band = bit32.band
else
	-- Lua 5.3
	band = load("local a, b = ... return a & b")
end

local function split(text, char)
	local groups = {}
	local lastGroupStart = 1
	for i = 1, #text do
		if text:sub(i, i) == char then
			table.insert(groups, text:sub(lastGroupStart, i - 1))
			lastGroupStart = i + 1
		end
	end
	table.insert(groups, text:sub(lastGroupStart))
	return groups
end

local function parseV4(text)
	local ipv4 = split(text, ".")
	assert(#ipv4 == 4, "IPv4 addresses are 4 bytes...")
	return string.char(
		tonumber(ipv4[1]),
		tonumber(ipv4[2]),
		tonumber(ipv4[3]),
		tonumber(ipv4[4])
	)
end

local function parse(text)
	-- :-breaking
	local groups = split(text, ":")
	-- Start-of-address :: normalization
	if groups[1] == "" then
		assert(groups[2] == "", "Address is prefixed with : (not ::)")
		table.remove(groups, 1)
	end
	-- End-of-address :: normalization
	-- If #groups == 1, then this would've been caught by the previous condition if it was invalid.
	if groups[#groups] == "" and #groups ~= 1 then
		assert(groups[#groups - 1] == "", "Address is suffixed with : (not ::)")
		table.remove(groups, #groups)
	end
	-- IPv4 Mapping
	local ok, ipv4 = pcall(parseV4, groups[#groups])
	if ok then
		-- Ok, it's valid
		table.remove(groups, #groups)
		table.insert(groups, string.format("%02x%02x", tonumber(ipv4:byte(1)), tonumber(ipv4:byte(2))))
		table.insert(groups, string.format("%02x%02x", tonumber(ipv4:byte(3)), tonumber(ipv4:byte(4))))
	end
	-- Zero Extension
	local zeroPoint
	for i = 1, #groups do
		if groups[i] == "" then
			assert(not zeroPoint, "Cannot be more than one zero point")
			zeroPoint = i
		end
	end
	if zeroPoint then
		groups[zeroPoint] = "0"
		while #groups < 8 do
			table.insert(groups, zeroPoint, "0")
		end
	end
	-- Final checks
	assert(#groups == 8, "IPv6 address must, after expansion, have 8 groups")
	local address = ""
	for i = 1, #groups do
		local group = tonumber("0x" .. groups[i])
		address = address .. string.char(math.floor(group / 256), group % 256)
	end
	return address
end

local function stringifyV4(address)
	return address:byte(1) .. "." .. address:byte(2) .. "." .. address:byte(3) .. "." .. address:byte(4)
end

local function stringify(address, ipv4Mapped)
	-- Automatically enable IPv4 mapping as per 'RECOMMENDED' clause in RFC 5952, Section 5.
	if ipv4Mapped == nil then
		if address:sub(1, 12) == "\x00\x00\x00\x00\x00\x00\x00\x00\xff\xff\xff\xff" then
			ipv4Mapped = true
		end
	end
	-- Bundle byte pairs into groups and search for largest zero run
	local groups = {}
	local j = 1
	local zeroStart
	local zeroCounter = 0
	local bestZeroStart
	local bestZeroCounter = 0
	for i = 1, #address, 2 do
		local n = (address:byte(i) * 0x100) + address:byte(i + 1)
		groups[j] = n
		if n == 0 then
			zeroStart = zeroStart or j
			zeroCounter = zeroCounter + 1
			if zeroCounter > bestZeroCounter then
				bestZeroStart = zeroStart
				bestZeroCounter = zeroCounter
			end
		else
			zeroStart = nil
			zeroCounter = 0
		end
		j = j + 1
	end
	-- Printing
	j = 1
	local text = ""
	local needToAddColon = false
	while j <= #groups do
		if j == 7 and ipv4Mapped then
			if needToAddColon then
				text = text .. ":"
			end
			text = text .. stringifyV4(address:sub(13))
			break
		end
		if j == bestZeroStart then
			j = j + bestZeroCounter
			-- ignore needToAddColon since it's required to add a colon here
			text = text .. "::"
			needToAddColon = false
		else
			if needToAddColon then
				text = text .. ":"
			end
			text = text .. string.format("%x", groups[j])
			needToAddColon = true
			j = j + 1
		end
	end
	return text
end

local function parseMask(text)
	local n = tonumber(text)
	if n then
		local mask = ""
		for i = 1, 16 do
			local power = 128
			local a = 0
			for j = 1, 8 do
				if n > 0 then
					a = a + power
					n = n - 1
				end
				power = power / 2
			end
			mask = mask .. string.char(a)
		end
		return mask
	else
		return parse(text)
	end
end

local function maskPrefixLength(mask)
	local bits = 0
	local ongoing = true
	for i = 1, 16 do
		local power = 128
		local a = mask:byte(i)
		for j = 1, 8 do
			if ongoing then
				if band(a, power) ~= 0 then
					bits = bits + 1
				else
					ongoing = false
				end
			else
				if band(a, power) ~= 0 then
					-- FAILED (discontinuous)
					return nil
				end
			end
			power = power / 2
		end
	end
	return bits
end

local function stringifyMask(mask)
	local prefixLength = maskPrefixLength(mask)
	if prefixLength then
		return tostring(prefixLength)
	end
	return stringify(mask, false)
end

return {
	-- Parses an IPv6 address according to RFC 4291.
	parse = parse,
	-- Parses an IPv4 address. This is useful for NAT64.
	parseV4 = parseV4,
	-- Stringifies an IPv6 address according to RFC 5952.
	stringify = stringify,
	-- Stringifies an IPv4 address. This is useful for NAT64.
	stringifyV4 = stringifyV4,
	-- Parses a network mask. This can be a number (prefix-length) or any valid IPv6 address format (mask).
	-- The usage of netmasks is NOT standard for IPv6 (the format is based on IPv4's), but is more flexible.
	parseMask = parseMask,
	-- Parses a prefix.
	parsePrefix = function (text)
		local groups = split(text, "/")
		if #groups > 1 then
			local prefixContent = table.remove(groups, #groups)
			return parse(table.concat(groups, "/")), parseMask(prefixContent)
		else
			return parse(text), parseMask("128")
		end
	end,
	-- If a mask is representable in prefix-length form, measure it.
	maskPrefixLength = maskPrefixLength,
	-- Stringifies a mask.
	stringifyMask = stringifyMask,
	stringifyPrefix = function (address, mask)
		return stringify(address) .. "/" .. stringifyMask(mask)
	end,
	-- Returns true if a given address matches an address/mask pair.
	matchesMask = function (value, targetAddr, targetMask)
		for i = 1, 16 do
			if band(value:byte(i), targetMask:byte(i)) ~= band(targetAddr:byte(i), targetMask:byte(i)) then
				return false
			end
		end
		return true
	end,
	generateLL = function (data)
		local vars = {0, 0, 0, 0, 0, 0, 0, 0}
		local p = 0
		for i = 1, #data do
			p = (p + data:byte(i)) % 8
			vars[p + 1] = (vars[p + 1] + data:byte(i)) % 256
		end
		return "\xfe\x80\x00\x00\x00\x00\x00\x00" .. string.char(table.unpack(vars))
	end,
	unspecified = "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
}

