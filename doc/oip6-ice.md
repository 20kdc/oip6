# Internet Component Emulation extensions

## TCP

### STANDARD inet.isTcpEnabled()

As in normal Internet components, returns a boolean.

### STANDARD inet.connect(address[, port])

See normal Inet component

*However,* 6to4 addressing is used if IPv4 addresses are given directly.

## EXT inet.listen(port)

Creates a port listener, or returns (nil, err).

NOTE: If a port listener already exists, this will fail. Please clean up resources properly.

See EXT: Port Listener API.

## HTTP

### STANDARD inet.isHttpEnabled()

As in normal Internet components, returns a boolean.

### STANDARD inet.request(...)

NYI

## Other

## EXT inet.hostAddress()

Returns the display form of the global IPv6 address of this computer.

If none is available, returns the link-local address.

## EXT inet.linkLocalAddress()

Returns the display form of the link-local IPv6 address of this computer.

# EXT: Port Listener API

## srv.id()

Returns the ID. This is used for internet\_ready alerts.

## srv.waiting()

If there is a waiting socket, returns the display form of the incoming address.

Otherwise, returns nil.

## srv.accept()

If there is a waiting socket, accept it (returns a TCP object, same as connect)

Otherwise, returns nil.

## srv.deny()

If there is a waiting socket, deny it.

Otherwise, does nothing.

## srv.close()

Stops listening on the port.

