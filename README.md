# oip6

IPv6 for the 'OpenComputers' Minecraft mod.

Also usable generally as a Lua IPv6 stack.

Need to write a more complete description here.

## Notice on API documentation

Only APIs with documents in doc/ are anywhere near being finished.

All libraries prefixed with `oip6i` are INTERNAL. DO NOT USE DIRECTLY.

## General Structure

The main 'meat' is in `common/`.

The code in `game/` is partially utilities, but also the services that
 connect everything up on various systems, along with utilities.

The `bench/` code is used for testing, hosting the system outside of the game.

The `native/` code is stuff that doesn't fit in `bench/` that is used outside of the game.

