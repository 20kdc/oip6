--
-- oip6-tcp: Transmission Control From User Space
--
-- Written starting in 2020.
--
-- To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
--

-- Creates a core TCP state machine.
--
-- Don't think too hard about specification-adherence with this one.
-- If you try reading said specification, you'll know why.
--
-- -- -- -- --
--
-- The state machine works with TCP packet tables as used in oip6-data,
--  assuming that header/srcPort/dstPort/windowSize are automatically set on sent packets,
--  and that the input is filtered by header/srcPort/dstPort.
--
-- For an inbound connection, ensure inbound is true,
--  and immediately provide the connection packet after creation.
-- For an outbound connection, ensure inbound is false.
-- The state machine will immediately send the connection packet.
--
-- Functions passed:
-- time() -> seconds, runAt(time, fn),
-- sendPacket(packet),
-- event("data", "BLAH")
-- event("debug", "TEST TEST")
-- event("close") -- on remote close
-- event("dead") -- on FIN after we have sent FIN, OR on remote close
--
-- This DOES NOT manage automatic closing of connections
--
-- Returns functions: onReceivePacket(packet), send(data), close()
return function (time, runAt, sendPacket, event, inbound)
	-- Variables --
	-- The current unconfirmed segment.
	-- The ACK parameters for this get modified, but NOTHING ELSE,
	--  or else we'll lose sequence number sync.
	local currentUnconfirmedSegmentPacket
	local transmitBuffer = ""
	local hasSentSyn = false
	local hasRecvSyn = false
	-- Indicates that a FIN should be *scheduled*
	local performingClose = false
	local hasSentFin = false
	local hasRecvFinRst = false
	local dead = false
	-- The local sequence number (aka our seqNum)
	local localSequenceNumber = 0
	-- The known remote sequence number (aka our ackNum)
	local remoteSequenceNumber = 0
	--

	local function sendSegment(packet)
		assert(not currentUnconfirmedSegmentPacket, "Can't send a segment while waiting on a segment ack!")
		currentUnconfirmedSegmentPacket = packet
		local wait = 2
		local function resender()
			if currentUnconfirmedSegmentPacket ~= packet then
				return
			end
			-- Keep this up to date.
			if packet.flags.ack then
				packet.ackNum = remoteSequenceNumber
			end
			sendPacket(packet)
			runAt(time() + wait, resender)
			wait = wait * 2
		end
		resender()
	end
	local function transmitDataIfAble()
		-- ONLY CALL sendSegment FROM HERE!
		if currentUnconfirmedSegmentPacket then return end

		if dead then
			-- Connection dead, DO NOT DO ANYTHING
		elseif not (inbound or hasSentSyn) then
			-- Send SYN (Outbound)
			-- (If inbound, we need to send a SYN+ACK, so that waits on hasRecvSyn.)
			sendSegment({
				seqNum = localSequenceNumber,
				-- We don't know this!
				ackNum = 0,
				flags = {syn = true},
				data = ""
			})
			localSequenceNumber = (localSequenceNumber + 1) % 0x100000000
			hasSentSyn = true
		elseif not hasRecvSyn then
			-- DO NOT GO PAST HERE WITHOUT RECEIVING A SYN --
		elseif not hasSentSyn then
			-- Send SYN (Inbound)
			sendSegment({
				seqNum = localSequenceNumber,
				ackNum = remoteSequenceNumber,
				flags = {syn = true, ack = true},
				data = ""
			})
			localSequenceNumber = (localSequenceNumber + 1) % 0x100000000
			hasSentSyn = true
		elseif transmitBuffer ~= "" then
			-- Data
			local effectiveWindow = 1220
			local seg = transmitBuffer:sub(1, effectiveWindow)
			transmitBuffer = transmitBuffer:sub(effectiveWindow + 1)
			if seg ~= "" then
				sendSegment({
					seqNum = localSequenceNumber,
					ackNum = remoteSequenceNumber,
					flags = {ack = true, psh = true},
					data = seg
				})
				localSequenceNumber = (localSequenceNumber + #seg) % 0x100000000
			end
		elseif performingClose and not hasSentFin then
			-- Close
			sendSegment({
				seqNum = localSequenceNumber,
				ackNum = remoteSequenceNumber,
				flags = {ack = true, fin = true},
				data = ""
			})
			localSequenceNumber = (localSequenceNumber + 1) % 0x100000000
			hasSentFin = true
		end
	end

	-- This is used to send the initial SYN if outbound.
	transmitDataIfAble()
	return function (packet)
		-- Receptor --
		-- NOTE: THE ORDER OF PROCESSING IS VERY IMPORTANT.

		-- If the packet starts at a sequence number that isn't this one,
		--  then it needs to be interesting by default to acknowledge we either
		--  already got something or already have something.
		-- This also gets set to true if actions are executed (to acknowledge)
		--  and controls if ACK will be sent if no packet is waiting to attach
		--  the number to.
		local packetInteresting = packet.seqNum ~= remoteSequenceNumber
		-- Current position in the packet.
		local ptrSeqNum = packet.seqNum

		-- ACK (Handle before transmitDataIfAble)
		if packet.flags.ack then
			-- If the remote TCP doesn't actually consume the whole packet,
			--  then there's unACKed stuff, isn't there?
			if packet.ackNum == localSequenceNumber then
				currentUnconfirmedSegmentPacket = nil
			end
		end

		-- SYN is only valid once.
		if packet.flags.syn then
			local e = (ptrSeqNum + 1) % 0x100000000
			if not hasRecvSyn then
				-- Contrary to TCP specification, it is a terrible idea to accept any SYN apart from the first one.
				-- The specified method of recovering from a duplicate SYN kills your connection.
				remoteSequenceNumber = e
				ptrSeqNum = remoteSequenceNumber
				hasRecvSyn = true
			end
			ptrSeqNum = e
			packetInteresting = true
		end

		-- RST
		if packet.flags.rst then
			-- Prevents more segments from being created, prevents blank ACKs
			if not dead then
				dead = true
				-- Shuts down existing transmission
				currentUnconfirmedSegmentPacket = nil
				if not hasRecvFinRst then
					hasRecvFinRst = true
					event("close", true)
				end
				event("dead")
			end
		end

		-- Try to find an alignment for data; if accurate, this forces packet sync on
		if packet.data ~= "" then
			local e = (ptrSeqNum + #packet.data) % 0x100000000
			for i = 1, #packet.data do
				-- Effective sequence number if the packet data starts from this point.
				local seqNumFromPoint = (packet.seqNum + (i - 1)) % 0x100000000
				if remoteSequenceNumber == seqNumFromPoint then
					-- Got it!
					local chk = packet.data:sub(i)
					remoteSequenceNumber = e
					event("data", chk)
					break
				end
			end
			ptrSeqNum = e
			packetInteresting = true
		end

		-- FIN
		if packet.flags.fin then
			local e = (ptrSeqNum + 1) % 0x100000000
			if remoteSequenceNumber == ptrSeqNum then
				remoteSequenceNumber = e
				if not hasRecvFinRst then
					hasRecvFinRst = true
					event("close", false)
					if hasSentFin then
						-- This FIN was initiated locally,
						--  so mark the connection as dead immediately
						dead = true
						event("dead")
					end
				end
			end
			ptrSeqNum = e
			packetInteresting = true
		end

		-- Finally, try to transmit.
		transmitDataIfAble()

		-- If there's no segment *after* trying to transmit, make a dedicated packet.
		-- Otherwise, the acknowledgement was sent in the last transmission.
		-- Also, don't do this if a RST has been received.
		if not (dead or currentUnconfirmedSegmentPacket) then
			if packetInteresting then
				sendPacket({
					seqNum = localSequenceNumber,
					ackNum = remoteSequenceNumber,
					flags = {ack = true},
					data = ""
				})
			end
		end
	end, function (data)
		if dead then
			return nil, "Dead"
		elseif performingClose then
			return nil, "Locally closing"
		end
		-- Send --
		transmitBuffer = transmitBuffer .. data
		transmitDataIfAble()
		return #data
	end, function ()
		-- Close --
		performingClose = true
		transmitDataIfAble()
	end
end
